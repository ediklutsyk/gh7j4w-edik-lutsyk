package com.geekhub.lessons.dao;

import com.geekhub.lessons.dto.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    void add(String name, String value);

    void update(String name, String value);

    void remove(String name, String value);

    void invalidate(String name, String value);

    User findByIdClass(Integer id);

    User findByIdMapper(Integer id);

    void insertList();
}
