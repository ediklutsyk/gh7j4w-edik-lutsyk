package com.geekhub.lessons.dao;

import com.geekhub.lessons.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Repository
public class UserDaoImp implements UserDao {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<User> findAll() {
        List<User> userList = jdbcTemplate.query("SELECT * FROM users", (rs, rowNum) -> {
            User user = new User();
            user.setId(rs.getInt("id"));
            user.setName(rs.getString("name"));
            user.setValue(rs.getString("value"));
            return user;
        });
        userList.sort(Comparator.comparing(User::getId));
        return userList;
    }

    @Override
    public void add(String name, String value) {
        jdbcTemplate.update("INSERT INTO users(name, value) VALUES(:name,:value);",
                new MapSqlParameterSource("name", name)
                        .addValue("value", value));
    }

    @Override
    public void update(String name, String value) {
        jdbcTemplate.update("UPDATE users SET value=:value WHERE name=:name;", new MapSqlParameterSource("name", name)
                .addValue("value", value));
    }

    @Override
    public void remove(String name, String value) {
        jdbcTemplate.update("DELETE FROM users WHERE name=:name;", new MapSqlParameterSource("name", name)
                .addValue("value", value));
    }

    @Override
    public void invalidate(String name, String value) {
        jdbcTemplate.update("DELETE FROM users;", new MapSqlParameterSource());
    }

    @Override
    public User findByIdClass(Integer id) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE id=:id;",
                new MapSqlParameterSource("id", id),
                new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public User findByIdMapper(Integer id) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE id=:id;",
                new MapSqlParameterSource("id", id),
                (rs, rowNum) -> {
                    User user = new User();
                    user.setId(rs.getInt("id"));
                    user.setName(rs.getString("name"));
                    user.setValue(rs.getString("value"));
                    return user;
                });
    }

    @Override
    public void insertList() {
        List<User> superHeroList = new ArrayList<>();
        superHeroList.add(new User("Tony", "Stark"));
        superHeroList.add(new User("Bruce", "Wayne"));
        jdbcTemplate.batchUpdate("INSERT INTO users (name, value) VALUES (:name, :value)",
                SqlParameterSourceUtils.createBatch(superHeroList.toArray()));
    }
}
