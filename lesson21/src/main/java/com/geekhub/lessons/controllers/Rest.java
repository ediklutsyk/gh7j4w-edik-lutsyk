package com.geekhub.lessons.controllers;

import com.geekhub.lessons.dto.User;
import com.geekhub.lessons.repository.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Rest {
    private final UserRepository userRepository;

    public Rest(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("insertUsers")
    public List<User> insertUsers() {
        return userRepository.insertUsersFromList();
    }

    @GetMapping("findUserByIdClass")
    public User findUserByIdClass(@RequestParam Integer id) {
        return userRepository.findByIdClass(id);
    }

    @GetMapping("findUserByIdMapper")
    public User findUserByIdMapper(@RequestParam Integer id) {
        return userRepository.findByIdMapper(id);
    }

    @RequestMapping(value = "exception")
    public void exception() {
        throw new RuntimeException("Some custom error");
    }
}
