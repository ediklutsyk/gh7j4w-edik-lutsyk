package com.geekhub.lessons.controllers;

import com.geekhub.lessons.repository.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    private final UserRepository userRepository;

    public HomeController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping("/home")
    public String index(Model model) {
        model.addAttribute("users", userRepository.getAllUsers());
        return "index";
    }

    @GetMapping(value = "session")
    public String toSession(@RequestParam("action") String action,
                            @RequestParam("name") String name,
                            @RequestParam("value") String value,
                            Model model) {
        model.addAttribute("users", userRepository.query(action, name, value));
        return "index";
    }

}
