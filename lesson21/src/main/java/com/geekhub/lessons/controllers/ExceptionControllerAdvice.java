package com.geekhub.lessons.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(
        basePackages = {"com.geekhub.lessons"},
        assignableTypes = {HomeController.class, Rest.class}
)
public class ExceptionControllerAdvice {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public ModelAndView exceptionHandler(Exception ex) {
        ModelAndView error = new ModelAndView("error");
        error.addObject("errorMessage", ex.getMessage());
        error.addObject("errorName", ex.getClass());
        return error;
    }
}