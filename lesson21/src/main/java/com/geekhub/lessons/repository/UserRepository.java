package com.geekhub.lessons.repository;

import com.geekhub.lessons.dao.UserDaoImp;
import com.geekhub.lessons.dto.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRepository {
    private final UserDaoImp userDaoImp;

    public UserRepository(UserDaoImp userDaoImp) {
        this.userDaoImp = userDaoImp;
    }

    public List<User> query(String action, String name, String value) {
        switch (action) {
            case "add":
                userDaoImp.add(name, value);
                break;
            case "update":
                userDaoImp.update(name, value);
                break;
            case "delete":
                userDaoImp.remove(name, value);
                break;
            case "invalidate":
                userDaoImp.invalidate(name, value);
                break;
        }
        return getAllUsers();
    }

    public List<User> insertUsersFromList() {
        userDaoImp.insertList();
        return getAllUsers();
    }

    public User findByIdMapper(Integer id) {
        return userDaoImp.findByIdMapper(id);
    }

    public User findByIdClass(Integer id) {
        return userDaoImp.findByIdClass(id);
    }

    public List<User> getAllUsers() {
        return userDaoImp.findAll();
    }

}
