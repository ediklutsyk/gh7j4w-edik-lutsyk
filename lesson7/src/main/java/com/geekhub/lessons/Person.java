package com.geekhub.lessons;

import java.util.Objects;

public class Person {
    private String firstName;
    private String lastName;
    private Universe universe;

    public Person(String firstName, String lastName, Universe universe) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.universe = universe;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Universe getUniverse() {
        return universe;
    }

    public Person() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                universe == person.universe;
    }

    @Override
    public int hashCode() {

        return Objects.hash(firstName, lastName, universe);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        if (firstName == null && lastName == null) {
            return "Person{}";
        } else {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }

    }
}
