package com.geekhub.lessons;

import com.geekhub.lessons.exceptions.AuthException;
import com.geekhub.lessons.exceptions.UserNotFoundException;
import com.geekhub.lessons.exceptions.WrongCredentialsException;
import com.geekhub.lessons.exceptions.WrongPasswordException;
import com.geekhub.lessons.interfaces.UserSource;
import com.geekhub.lessons.license.License;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

public class DbUserSource implements UserSource {

    private User currentUser;
    private DriverManagerDataSource dataSource;

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void auth(String login, String password) throws AuthException {
        if (login.isEmpty() || password.isEmpty()) {
            throw new WrongCredentialsException("The username or password is empty ");
        }
        User user;
        if (findByLogin(login).isPresent()) {
            user = findByLogin(login).get();
        } else {
            throw new UserNotFoundException("There is no such user");
        }
        if (!user.getPassword().equals(password)) {
            throw new WrongPasswordException("The password is not correct");
        }
        this.currentUser = user;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public Collection<User> getUsers() {
        Collection<User> userCollection = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery("SELECT * FROM Users");
            while (rs.next()) {
                User user = createNewUserInstance(connection, rs);
                userCollection.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException("An exception during getting all users from DB");
        }
        return userCollection;
    }

    @Override
    public void addUser(User user) {
        Objects.requireNonNull(user);
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO Users(login,password,fullName,admin) VALUES (?, ?, ?,?)");
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getFullName());
            ps.setBoolean(4, user.isAdmin());
            ps.executeUpdate();
            user.getLicenses().forEach(license -> {
                addLicence(user.getId(), license);
            });

        } catch (SQLException e) {
            throw new RuntimeException("An exception during adding new user to DB");
        }
    }

    @Override
    public void addLicence(Integer id, License license) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO Licenses(id,type,startDate,expirationDate) VALUES (?,?,?,?)");
            ps.setInt(1, id);
            ps.setString(2, license.getType());
            ps.setDate(3, Date.valueOf(license.getStartDate()));
            ps.setDate(4, Date.valueOf(license.getExpirationDate()));
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("An exception during adding license to DB");
        }
    }

    @Override
    public void removeUser(User user) {
        Objects.requireNonNull(user);
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement deleteLicensesStatement = connection.prepareStatement(
                    "DELETE FROM licenses l USING Users u WHERE l.id = u.id AND u.login = ?;");
            PreparedStatement deleteUserStatement = connection.prepareStatement("DELETE FROM Users u WHERE u.login = ?;");
            deleteLicensesStatement.setString(1, user.getLogin());
            deleteUserStatement.setString(1, user.getLogin());
            deleteLicensesStatement.executeUpdate();
            deleteUserStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("An exception during removing new user from DB");
        }
    }

    @Override
    public Optional<User> findByLogin(String login) {
        User user = null;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Users WHERE login=?");
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createNewUserInstance(connection, rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException("An exception during search user by login in DB");
        }
        return Optional.ofNullable(user);
    }

    @Override
    public void updateUser(User user) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("UPDATE users SET password=?, fullName=?, admin=? WHERE login=?");
            ps.setString(1, user.getPassword());
            ps.setString(2, user.getFullName());
            ps.setBoolean(3, user.isAdmin());
            ps.setString(4, user.getLogin());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("An exception during updating users in DB");
        }
    }

    private User createNewUserInstance(Connection connection, ResultSet rs) throws SQLException {
        User user;
        user = new User(rs.getInt("id"),
                rs.getString("login"),
                rs.getString("password"),
                rs.getString("fullName"),
                rs.getBoolean("admin"));
        getUserLicensesFromDb(user, connection, rs.getInt("id"));
        return user;
    }

    private void getUserLicensesFromDb(User user, Connection connection, int id) throws SQLException {
        PreparedStatement licensePreparedStatement = connection.prepareStatement("SELECT * FROM licenses WHERE id=?");
        licensePreparedStatement.setInt(1, id);
        ResultSet preparedResultSet = licensePreparedStatement.executeQuery();
        while (preparedResultSet.next()) {
            user.addLicense(new License(preparedResultSet.getString("type"),
                    preparedResultSet.getDate("startDate").toLocalDate(),
                    preparedResultSet.getDate("expirationDate").toLocalDate()));
        }
    }

}
