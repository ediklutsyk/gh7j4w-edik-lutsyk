package com.geekhub.lessons.license;

import java.time.LocalDate;
import java.util.Objects;

public class License {
    private String type;
    private final LocalDate startDate;
    private final LocalDate expirationDate;
    private LocalDate today = LocalDate.now();


    public License(String type, LocalDate startDate, LocalDate expirationDate) {
        Objects.requireNonNull(startDate);
        Objects.requireNonNull(expirationDate);
        if (startDate.isAfter(expirationDate)) {
            throw new RuntimeException("Start date should be smaller than expiration");
        }
        this.type = type;
        this.startDate = startDate;
        this.expirationDate = expirationDate;
    }

    public String getType() {
        return type;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public boolean nonExpired() {
        if (today.equals(this.getStartDate()) || today.equals(this.getExpirationDate())) {
            return true;
        } else if (!today.isBefore(startDate) && !today.isAfter(expirationDate)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "License{" +
                "type=" + type +
                ", startDate=" + startDate +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
