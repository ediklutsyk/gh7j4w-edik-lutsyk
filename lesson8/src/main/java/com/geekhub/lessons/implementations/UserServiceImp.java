package com.geekhub.lessons.implementations;

import com.geekhub.lessons.DbUserSource;
import com.geekhub.lessons.User;
import com.geekhub.lessons.interfaces.UserService;
import com.geekhub.lessons.UserValidator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserServiceImp implements UserService {
    private final DbUserSource userSource;
    private final UserValidator userValidator;

    public UserServiceImp(DbUserSource userSource, UserValidator userValidator) {
        this.userSource = userSource;
        this.userValidator = userValidator;
    }

    @Override
    public boolean isCurrentUserAdmin() {
        return userSource.getCurrentUser().isAdmin();
    }

    @Override
    public User getCurrentUser() {
        return userSource.getCurrentUser();
    }

    @Override
    public void saveUser(User user) {
        Objects.requireNonNull(user);
        userValidator.validateUser(user);
        userSource.addUser(user);
    }

    @Override
    public void removeUser(String login) {
        userSource.removeUser(userSource.findByLogin(login).orElseThrow(() -> new RuntimeException("No such user to remove")));
    }

    @Override
    public void updateUser(User user) {
        Objects.requireNonNull(user);
        userSource.updateUser(user);
    }


    @Override
    public List<User> findAllOrderByFullNameAndLogin() {
        return userSource.getUsers().stream()
                .sorted(Comparator.comparing(User::getFullName)
                        .thenComparing(User::getLogin))
                .collect(Collectors.toList());
    }

    @Override
    public List<User> findAllUsersWhoseLicensesExpireInSetNumberOfDays(int numberOfDays) {
        LocalDate today = LocalDate.now();
        return userSource.getUsers().stream()
                .filter(user -> ChronoUnit.DAYS.between(
                        today, user.getActiveLicense()
                                .orElseThrow(() -> new RuntimeException("User have no active license"))
                                .getExpirationDate()) == numberOfDays)
                .collect(Collectors.toList());
    }


}
