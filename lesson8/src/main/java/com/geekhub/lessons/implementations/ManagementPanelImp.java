package com.geekhub.lessons.implementations;

import com.geekhub.lessons.interfaces.ManagementPanel;
import com.geekhub.lessons.User;
import com.geekhub.lessons.interfaces.UserService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ManagementPanelImp implements ManagementPanel {
    private final UserService userService;

    public ManagementPanelImp(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void createNewUser(User user) {
        if (userService.isCurrentUserAdmin()) {
            userService.saveUser(user);
        } else {
            throw new RuntimeException("Only admin can save users");
        }
    }

    @Override
    public void updateUser(User user) {
        if (userService.isCurrentUserAdmin()) {
            userService.updateUser(user);
        } else {
            throw new RuntimeException("Only admin can update users");
        }

    }

    @Override
    public void removeUser(String login) {
        if (userService.isCurrentUserAdmin()) {
            userService.removeUser(login);
        } else if (userService.getCurrentUser().getLogin().equals(login)) {
            throw new RuntimeException("You cannot remove current user");
        } else {
            throw new RuntimeException("Only admin can remove users");
        }
    }

    @Override
    public void printAllSortedByFullNameAndLogin() {
        System.out.println(userService.findAllOrderByFullNameAndLogin());
    }


    @Override
    public Map<Boolean, List<User>> adminsMap() {
        return userService.findAllOrderByFullNameAndLogin().stream()
                .collect(Collectors.partitioningBy(User::isAdmin));
    }

}
