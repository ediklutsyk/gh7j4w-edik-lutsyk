package com.geekhub.lessons.exceptions;

public class WrongCredentialsException extends AuthException {
    public WrongCredentialsException(String message) {
        super(message);
    }
}
