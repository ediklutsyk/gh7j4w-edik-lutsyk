package com.geekhub.lessons.exceptions;

public class WrongPasswordException extends WrongCredentialsException {
    public WrongPasswordException(String message) {
        super(message);
    }
}
