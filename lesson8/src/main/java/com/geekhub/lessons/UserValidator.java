package com.geekhub.lessons;

import com.geekhub.lessons.interfaces.UserSource;

import java.util.Set;
import java.util.stream.Collectors;

public class UserValidator {
    private final UserSource userSource;

    public UserValidator(UserSource userSource) {
        this.userSource = userSource;
    }

    public void validateUser(User user) {
        checkLicensePresence(user);
        checkActiveLicense(user);
        checkLoginAvailability(user);
        checkIdAvailability(user);
    }

    private void checkLicensePresence(User user) {
        if (user.getLicenses().isEmpty()) {
            throw new RuntimeException("User should have license");
        }
    }

    private void checkActiveLicense(User user) {
        boolean hasActiveLicense = user.getActiveLicense().isPresent();
        if (!hasActiveLicense) {
            throw new RuntimeException("User should have active license");
        }
    }

    private void checkLoginAvailability(User user) {
        Set<String> usedLogins = userSource.getUsers().stream()
                .map(User::getLogin)
                .collect(Collectors.toSet());
        boolean loginIsUsed = usedLogins.contains(user.getLogin());
        if (loginIsUsed) {
            throw new RuntimeException("User should have unique login");
        }
    }

    private void checkIdAvailability(User user) {
        Set<Integer> usedIds = userSource.getUsers().stream()
                .map(User::getId)
                .collect(Collectors.toSet());
        boolean idIsUsed = usedIds.contains(user.getId());
        if (idIsUsed) {
            throw new RuntimeException("User should have unique ID");
        }
    }
}
