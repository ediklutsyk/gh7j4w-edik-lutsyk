package com.geekhub.lessons;

import com.geekhub.lessons.license.License;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class User {
    private final int id;
    private String login;
    private String password;
    private final String fullName;
    private boolean admin;
    private final List<License> licenses;

    public User(int id, String login, String password, String fullName, boolean admin) {
        this.id = id;
        this.login = Objects.requireNonNull(login);
        this.password = password;
        this.fullName = Objects.requireNonNull(fullName);
        this.licenses = new ArrayList<>();
        this.admin = admin;
    }

    @Override
    public String toString() {
        String lastName = fullName.substring(fullName.indexOf(' ') + 1, fullName.length());
        if (getActiveLicense().isPresent()) {
            return "User{" +
                    "Last name='" + lastName +
                    ", login='" + login + '\'' +
                    ", Licenses{Type:" + getActiveLicense().get().getType() +
                    ", expiration date='" + getActiveLicense().get().getExpirationDate() + '\'' +
                    ", license date left='" + ChronoUnit.DAYS.between(
                    LocalDate.now(), getActiveLicense().get().getExpirationDate()) + '\'' +
                    '}';
        } else {
            return "User{" +
                    "Last name='" + lastName +
                    ", login='" + login + '\'' +
                    ", Licenses{INACTIVE}";
        }

    }

    int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void addLicense(License license) {
        licenses.add(license);
    }

    public Optional<License> getActiveLicense() {
        return getLicenses().stream()
                .filter(License::nonExpired)
                .findFirst();
    }

    public List<License> getLicenses() {
        return new ArrayList<>(licenses);
    }

}