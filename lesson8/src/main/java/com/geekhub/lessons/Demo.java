package com.geekhub.lessons;

import com.geekhub.lessons.exceptions.AuthException;
import com.geekhub.lessons.exceptions.UserNotFoundException;
import com.geekhub.lessons.exceptions.WrongCredentialsException;
import com.geekhub.lessons.implementations.ManagementPanelImp;
import com.geekhub.lessons.implementations.UserServiceImp;
import com.geekhub.lessons.interfaces.ManagementPanel;
import com.geekhub.lessons.interfaces.UserSource;
import com.geekhub.lessons.license.License;
import com.geekhub.lessons.logger.LoggerService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Demo {
    private static List<Exception> systemExceptionList = new ArrayList<>();

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        LoggerService loggerService = context.getBean(LoggerService.class);
        DbUserSource userSource = context.getBean(DbUserSource.class);
        UserValidator userValidator = new UserValidator(userSource);
        UserServiceImp userService = new UserServiceImp(userSource, userValidator);
        ManagementPanel managementPanel = new ManagementPanelImp(userService);

        auth(userSource, "CurrentUser", "password", loggerService);
        loggerService.print("Current user:" + context.getBean(DbUserSource.class).getCurrentUser());

        loggerService.print("Create users:");
        User fourthUser = new User(4, "FourthUser", "password4", "FullName FourthUser", false);
        fourthUser.addLicense(new License("WEB_ONLY",
                LocalDate.of(2018, 11, 19),
                LocalDate.of(2018, 11, 25)));
        User fifthUser = new User(4, "FifthUser", "password5", "FullName FifthUser", false);
        fifthUser.addLicense(new License("WEB_ONLY",
                LocalDate.of(2018, 2, 15),
                LocalDate.of(2018, 4, 15)));

        demoOfCreateUserMethod(fourthUser, managementPanel, "Fourth User Exception:", loggerService);
        demoOfCreateUserMethod(fifthUser, managementPanel, "Fifth User Exception:", loggerService);


        loggerService.print('\n' + "Print all sorted by full name and login:");
        demoOfPrintAllSortedByFullNameAndLoginMethod(managementPanel, loggerService);

        loggerService.print('\n' + "Removed Second user");
        demoOfRemoveUserMethod(managementPanel, loggerService);

        loggerService.print('\n' + "Print all sorted by full name and login:");
        demoOfPrintAllSortedByFullNameAndLoginMethod(managementPanel, loggerService);

        loggerService.print('\n' + "All admins users:");
        demoOfAdminsMapMethod(managementPanel, loggerService);

        loggerService.print('\n' + "Find all users who's licenses expire in set number of days :");
        demoOfFindAllUsersWhoseLicensesExpireInSetNumberOfDaysMethod(userService.findAllUsersWhoseLicensesExpireInSetNumberOfDays(58), loggerService);

        loggerService.print('\n' + "Update user: ");
        demoOfUpdateMethod(managementPanel, userSource, loggerService);

        loggerService.print('\n' + "All exceptions:");
        systemExceptionList.forEach(loggerService::print);

    }

    private static void auth(UserSource userSource, String login, String password, LoggerService loggerService) {
        try {
            userSource.auth(login, password);
        } catch (UserNotFoundException | WrongCredentialsException e) {
            systemExceptionList.add(e);
            loggerService.print(e.getMessage());
        } catch (AuthException e) {
            loggerService.print(e.getMessage());
        }
    }

    private static void demoOfRemoveUserMethod(ManagementPanel managementPanel, LoggerService loggerService) {
        try {
            managementPanel.removeUser("SecondUser");
        } catch (RuntimeException e) {
            systemExceptionList.add(e);
            loggerService.print(e.getMessage());
        }
    }

    private static void demoOfPrintAllSortedByFullNameAndLoginMethod(ManagementPanel managementPanel, LoggerService loggerService) {
        try {
            managementPanel.printAllSortedByFullNameAndLogin();
        } catch (RuntimeException e) {
            systemExceptionList.add(e);
            loggerService.print(e.getMessage());
        }
    }

    private static void demoOfAdminsMapMethod(ManagementPanel managementPanel, LoggerService loggerService) {
        try {
            loggerService.print(managementPanel.adminsMap());
        } catch (RuntimeException e) {
            systemExceptionList.add(e);
            loggerService.print(e.getMessage());
        }
    }


    private static void demoOfFindAllUsersWhoseLicensesExpireInSetNumberOfDaysMethod(
            List<User> allUsersWhoseLicensesExpireInSetNumberOfDays, LoggerService loggerService) {
        try {
            loggerService.print(allUsersWhoseLicensesExpireInSetNumberOfDays);
        } catch (RuntimeException e) {
            systemExceptionList.add(e);
            loggerService.print(e.getMessage());
        }
    }


    private static void demoOfUpdateMethod(ManagementPanel managementPanel, UserSource userSource, LoggerService loggerService) {
        try {
            loggerService.print(userSource.findByLogin("FifthUser"));
            User updatedFifthUser =
                    new User(5, "FifthUser", "qwerty12345", "FullName NewFifthUser", false);
            managementPanel.updateUser(updatedFifthUser);
            loggerService.print(userSource.findByLogin("FifthUser"));
        } catch (RuntimeException e) {
            systemExceptionList.add(e);
            loggerService.print(e.getMessage());
        }
    }


    private static void demoOfCreateUserMethod(User user, ManagementPanel managementPanel,
                                               String exceptionMessage, LoggerService loggerService) {
        try {
            managementPanel.createNewUser(user);
        } catch (RuntimeException e) {
            systemExceptionList.add(e);
            loggerService.print(exceptionMessage + e.getMessage());
        }
    }
}
