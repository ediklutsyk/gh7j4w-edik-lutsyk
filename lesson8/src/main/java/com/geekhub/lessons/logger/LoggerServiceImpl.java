package com.geekhub.lessons.logger;

public class LoggerServiceImpl implements LoggerService {
    @Override
    public void print(Object object) {
        System.out.println(String.valueOf(object));
    }
}
