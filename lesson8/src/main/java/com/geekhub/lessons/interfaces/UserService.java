package com.geekhub.lessons.interfaces;

import com.geekhub.lessons.User;

import java.util.List;

public interface UserService {
    boolean isCurrentUserAdmin();

    User getCurrentUser();

    void saveUser(User user);

    void removeUser(String login);

    void updateUser(User user);

    List<User> findAllOrderByFullNameAndLogin();

    List<User> findAllUsersWhoseLicensesExpireInSetNumberOfDays(int numberOfDays);

}