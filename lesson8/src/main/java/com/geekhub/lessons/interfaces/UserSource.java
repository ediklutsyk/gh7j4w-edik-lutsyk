package com.geekhub.lessons.interfaces;

import com.geekhub.lessons.User;
import com.geekhub.lessons.exceptions.AuthException;
import com.geekhub.lessons.license.License;

import java.util.Collection;
import java.util.Optional;

public interface UserSource {

    void auth(String login, String password) throws AuthException;

    Collection<User> getUsers();

    void addUser(User user);

    void addLicence(Integer id, License license);

    void removeUser(User user);

    Optional<User> findByLogin(String login);

    void updateUser(User user);
}
