package com.geekhub.lessons.interfaces;

import com.geekhub.lessons.User;

import java.util.List;
import java.util.Map;

public interface ManagementPanel {
    void createNewUser(User user);

    void updateUser(User user);

    void removeUser(String login);

    void printAllSortedByFullNameAndLogin();

    Map<Boolean, List<User>> adminsMap();

}
