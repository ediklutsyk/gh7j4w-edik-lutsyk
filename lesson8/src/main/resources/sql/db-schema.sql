DROP TABLE IF EXISTS Users;
CREATE TABLE Users(
   id SMALLSERIAL PRIMARY KEY,
   login VARCHAR (50),
   password VARCHAR (50),
   fullName VARCHAR (50),
   admin BOOLEAN
);
DROP TABLE IF EXISTS Licenses;
CREATE TABLE Licenses(
   id INT,
   type VARCHAR (50),
   startDate DATE,
   expirationDate DATE
);
