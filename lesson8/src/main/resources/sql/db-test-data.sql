INSERT INTO Users(login,password,fullName,admin) VALUES ('CurrentUser', 'password', 'FullName CurrentUser',TRUE);
INSERT INTO Users(login,password,fullName,admin) VALUES ('SecondUser', 'password2', 'FullName SecondUser',FALSE);
INSERT INTO Users(login,password,fullName,admin) VALUES ('ThirdUser', 'password3', 'FullName ThirdUser',FALSE);

INSERT INTO Licenses(id,type,startDate,expirationDate) VALUES (1,'FULL', '2018-01-01', '2019-01-01');
INSERT INTO Licenses(id,type,startDate,expirationDate) VALUES (1,'FULL', '2019-01-02', '2020-01-02');
INSERT INTO Licenses(id,type,startDate,expirationDate) VALUES (2,'WEB_ONLY', '2018-01-01', '2019-08-28');
INSERT INTO Licenses(id,type,startDate,expirationDate) VALUES (3,'MOBILE_ONLY', '2018-01-01', '2019, 1, 1');
