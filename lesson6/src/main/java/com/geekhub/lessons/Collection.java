package com.geekhub.lessons;

import java.util.*;
import java.util.function.*;
import java.util.stream.Stream;

public class Collection implements CollectionOperations {
    @Override
    public <E> List<E> fill(Supplier<E> producer, int count) {
        List list = new ArrayList();
        for (int i = 0; i < count; i++) {
            list.add(producer.get());
        }
        return list;
    }

    @Override
    public <E> List<E> filter(List<E> elements, Predicate<E> predicate) {
        List<E> list = new ArrayList<>();
        elements.forEach(item -> {
            if (predicate.test(item)) {
                list.add(item);
            }
        });
        return list;
    }

    @Override
    public <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E item : elements) {
            if (predicate.test(item)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E item : elements) {
            if (!predicate.test(item)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E item : elements) {
            if (predicate.test(item)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> list = new ArrayList<>();
        elements.forEach(item -> {
            list.add(mappingFunction.apply(item));
        });
        return list;
    }

    @Override
    public <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()){
            return Optional.empty();
        }
        List<E> copyList = elements;
        copyList.sort(comparator);
        return Optional.ofNullable(copyList.get(elements.size() - 1));
    }

    @Override
    public <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()){
            return Optional.empty();
        }
        List<E> copyList = elements;
        copyList.sort(comparator);
        return Optional.ofNullable(copyList.get(0));
    }

    @Override
    public <E> List<E> distinct(List<E> elements) {
        Set<E> set = new HashSet<>(elements);
        return new ArrayList<>(set);
    }

    @Override
    public <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (int i = 0; i < elements.size(); i++) {
            consumer.accept(elements.get(i));
        }

    }

    @Override
    public <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()){
            return Optional.empty();
        }
        E result = elements.get(0);
        for (E element : elements) {
            result = accumulator.apply(result, element);
        }
        return Optional.of(result);
    }

    @Override
    public <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;
        for (E element : elements) {
            result = accumulator.apply(result, element);
        }
        return result;
    }

    @Override
    public <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        Map<Boolean, List<E>> map = new HashMap<>();
        List<E> listTrue = new ArrayList<>();
        List<E> listFalse = new ArrayList<>();
        elements.forEach(item -> {
            if (predicate.test(item)) {
                listTrue.add(item);
            } else {
                listFalse.add(item);
            }
        });
        map.put(true, listTrue);
        map.put(false, listFalse);
        return map;
    }

    @Override
    public <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> map = new HashMap<>();
        Set<K> categoriesSet = new HashSet<>();
        elements.forEach(item -> {
            categoriesSet.add(classifier.apply(item));
        });
        categoriesSet.forEach(category -> {
            List<T> list = new ArrayList<>();
            elements.forEach(person -> {
                if (classifier.apply(person).equals(category)) {
                    list.add(person);
                }
            });
            map.put(category, list);
        });
        return map;

    }

    @Override
    public <T, K, U> Map<K, U> toMap(List<T> elements, Function<T, K> keyFunction, Function<T, U> valueFunction, BinaryOperator<U> mergeFunction) {
        Map<K, U> map = new HashMap<>();
        for (T element : elements) {
            K key = keyFunction.apply(element);
            if (map.containsKey(key)) {
                map.put(key, mergeFunction.apply(map.get(key), valueFunction.apply(element)));
            } else {
                map.put(key, valueFunction.apply(element));
            }
        }
        return map;
    }
}
