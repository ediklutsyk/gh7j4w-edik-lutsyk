package com.geekhub.lessons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

import static com.geekhub.lessons.Universe.*;

public class Demo {

    public static void main(String[] args) {
        CollectionOperations coll = new Collection();
        List<Person> peopleList = new ArrayList<>();
        peopleList.add(new Person("Tony", "Stark"));
        peopleList.add(new Person("Tor", "Odinson"));
        peopleList.add(new Person("Stephen", "Strange"));
        peopleList.add(new Person("Bruce", "Wayne"));
        peopleList.add(new Person("Slade", "Wilson"));

        List<Person> peopleListWithUniverse = new ArrayList<>();
        peopleListWithUniverse.add(new Person("Tony", "Stark", MARVEL));
        peopleListWithUniverse.add(new Person("Tor", "Odinson", MARVEL));
        peopleListWithUniverse.add(new Person("Stephen", "Strange", MARVEL));
        peopleListWithUniverse.add(new Person("Bruce", "Wayne", DC));
        peopleListWithUniverse.add(new Person("Slade", "Wilson", DC));

        System.out.println("Demo of the fill method:");
        fillMethod(coll);

        System.out.println("\nDemo of the filter method:");
        filterMethod(coll, peopleList);

        System.out.println("\nDemo of the anyMatch method:");
        anyMatchMethod(coll, peopleList);

        System.out.println("\nDemo of the allMatch method:");
        allMatchMethod(coll, peopleList);

        System.out.println("\nDemo of the noneMatch method:");
        noneMetchMethod(coll, peopleList);

        System.out.println("\nDemo of the map method:");
        Function<Person, String> lastNameFuncLambda = mapMethod(coll, peopleList);

        System.out.println("\nDemo of the max and min methods:");
        maxAndMinMethod(coll, peopleList);

        System.out.println("\nDemo of the distinct method:");
        distinctMethod(coll);

        System.out.println("\nDemo of the forEach method:");
        forEachMethod(coll, peopleList);

        System.out.println("\nDemo of the reduce method:");
        reduceMethod(coll);

        System.out.println("\nDemo of the partitionBy method:");
        Predicate<Person> personPredicateForMatchesLambda = (name) -> name.getFirstName().charAt(0) == 'T';
        System.out.println(coll.partitionBy(peopleList, personPredicateForMatchesLambda));

        System.out.println("\nDemo of the groupBy method:");
        Function<Person, Universe> universeFunctionLambda = groupByMethod(coll, peopleListWithUniverse);

        System.out.println("\nDemo of the toMap method:");
        toMapMethod(coll, peopleListWithUniverse, lastNameFuncLambda, universeFunctionLambda);
    }

    private static void toMapMethod(CollectionOperations coll, List<Person> peopleListWithUniverse, Function<Person, String> lastNameFuncLambda, Function<Person, Universe> universeFunctionLambda) {
        BinaryOperator<String> stringBinaryOperatorAbstract = new BinaryOperator<String>() {
            @Override
            public String apply(String s1, String s2) {
                return s1 + " " + s2 + " ";
            }
        };
        System.out.println(coll.toMap(peopleListWithUniverse, universeFunctionLambda, lastNameFuncLambda, stringBinaryOperatorAbstract));
    }

    private static Function<Person, Universe> groupByMethod(CollectionOperations coll, List<Person> peopleListWithUniverse) {
        Function<Person, Universe> universeFunctionAbstract = new Function<Person, Universe>() {
            @Override
            public Universe apply(Person person) {
                return person.getUniverse();
            }
        };
        Function<Person, Universe> universeFunctionLambda = person -> person.getUniverse();
        System.out.println(coll.groupBy(peopleListWithUniverse, universeFunctionAbstract));
        return universeFunctionLambda;
    }

    private static void reduceMethod(CollectionOperations coll) {
        BinaryOperator<Integer> adderAbstract = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer n1, Integer n2) {
                return n1 + n2;
            }
        };
        BinaryOperator<Integer> adderLambda = (n1, n2) -> n1 + n2;
        System.out.println(coll.reduce(Arrays.asList(1, 2, 5, 1, 2, 4, 5, 1, 3), adderLambda));
        System.out.println("Demo of the reduce with seed method:");
        System.out.println(coll.reduce(8, Arrays.asList(1, 2, 5, 1, 2, 4, 5, 1, 3), adderLambda));
    }

    private static void forEachMethod(CollectionOperations coll, List<Person> peopleList) {
        Consumer<Person> personConsumerAbstract = new Consumer<Person>() {
            @Override
            public void accept(Person person) {
                System.out.print(person.getFirstName() + ',');
            }
        };
        Consumer<Person> personConsumerLambda = person -> System.out.print(person.getFirstName() + ',');
        coll.forEach(peopleList, personConsumerAbstract);
        coll.forEach(peopleList, personConsumerLambda);
    }

    private static void distinctMethod(CollectionOperations coll) {
        System.out.println(coll.distinct(Arrays.asList(1, 2, 5, 1, 2, 4, 5, 1, 3)));
    }

    private static void maxAndMinMethod(CollectionOperations coll, List<Person> peopleList) {
        Comparator<Person> personCompareByName = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                int result = o1.getFirstName().length() - o2.getFirstName().length();
                if (result == 0) {
                    return 0;
                } else if (result > 0) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
        System.out.println(coll.max(peopleList, personCompareByName));
        System.out.println(coll.min(peopleList, personCompareByName));
    }

    private static Function<Person, String> mapMethod(CollectionOperations coll, List<Person> peopleList) {
        Function<Person, String> lastNameFuncAbstract = new Function<Person, String>() {
            @Override
            public String apply(Person person) {
                return person.getLastName();
            }

        };
        Function<Person, String> lastNameFuncLambda = person -> person.getLastName();
        System.out.println(coll.map(peopleList, lastNameFuncAbstract));
        System.out.println(coll.map(peopleList, lastNameFuncLambda));
        return lastNameFuncLambda;
    }

    private static void noneMetchMethod(CollectionOperations coll, List<Person> peopleList) {
        Predicate<Person> personPredicateForNoneMatchesLambda = (name) -> name.getFirstName().length() > 8;
        System.out.println(coll.noneMatch(peopleList, personPredicateForNoneMatchesLambda));
    }

    private static void  allMatchMethod(CollectionOperations coll, List<Person> peopleList){
        Predicate<Person> personPredicateForMatchesLambda = (name) -> name.getFirstName().charAt(0) == 'T';
        Predicate<Person> personPredicateLambda = (name) -> name.getFirstName().length() < 5;
        List<Person> peopleWithNameLessThanFive = coll.filter(peopleList, personPredicateLambda);
        System.out.println(coll.allMatch(peopleWithNameLessThanFive, personPredicateForMatchesLambda));
        System.out.println(coll.allMatch(peopleList, personPredicateForMatchesLambda));
        System.out.println(coll.allMatch(peopleWithNameLessThanFive, personPredicateForMatchesLambda));
        System.out.println(coll.allMatch(peopleList, personPredicateForMatchesLambda));
    }

    private static void anyMatchMethod(CollectionOperations coll, List<Person> peopleList){
        Predicate<Person> personPredicateForMatchesAbstract = new Predicate<Person>() {
            @Override
            public boolean test(Person person) {
                if (person.getFirstName().charAt(0) == 'T') {
                    return true;
                }
                return false;
            }
        };
        Predicate<Person> personPredicateForMatchesLambda = (name) -> name.getFirstName().charAt(0) == 'T';
        System.out.println(coll.anyMatch(peopleList, personPredicateForMatchesAbstract));
        System.out.println(coll.anyMatch(peopleList, personPredicateForMatchesLambda));
    }

    private static void filterMethod(CollectionOperations coll, List<Person> peopleList) {
        Predicate<Person> personPredicateAbstract = new Predicate<Person>() {
            @Override
            public boolean test(Person person) {
                if (person.getFirstName().length() < 5) {
                    return true;
                }
                return false;
            }
        };
        Predicate<Person> personPredicateLambda = (name) -> name.getFirstName().length() < 5;
        List<Person> peopleWithNameLessThanFive = coll.filter(peopleList, personPredicateAbstract);
        System.out.println(peopleWithNameLessThanFive);
        System.out.println(coll.filter(peopleList, personPredicateLambda));
    }

    private static void fillMethod(CollectionOperations coll) {
        Supplier<Person> personSupplierAbstract = new Supplier<Person>() {
            @Override
            public Person get() {
                return new Person();
            }
        };
        Supplier<Person> personSupplierReferens = Person::new;
        Supplier<Person> personSupplierLambda = () -> new Person();
        System.out.println(coll.fill(personSupplierAbstract, 5));
        System.out.println(coll.fill(personSupplierReferens, 5));
        System.out.println(coll.fill(personSupplierLambda, 5));
    }

}
