package com.geekhub.lessons;

public class Person {
    private String firstName;
    private String lastName;
    private Universe universe;

    public Person(String firstName, String lastName, Universe universe) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.universe = universe;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Universe getUniverse() {
        return universe;
    }

    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        if (firstName == null && lastName == null) {
            return "Person{}";
        } else {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }

    }
}
