package com.geekhub.lessons.task2;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ChangeSet {
    private List oldValues;
    private List newValues;
    public Set<Object> changeSet = new HashSet<>();

    public ChangeSet() {
    }

    public void add(Object o) {
        this.changeSet.add(o);
    }

    public List getOldValues() {
        return oldValues;
    }

    public List getNewValues() {
        return newValues;
    }

    public void setOldValues(List oldValues) {
        this.oldValues = oldValues;
    }

    public void setNewValues(List newValues) {
        this.newValues = newValues;
    }

    @Override
    public String toString() {
        return "ChangeSet: " + changeSet;
    }
}
