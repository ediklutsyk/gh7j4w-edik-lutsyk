package com.geekhub.lessons.task2.license;

public enum LicenseType {
    FULL, WEB_ONLY, MOBILE_ONLY
}
