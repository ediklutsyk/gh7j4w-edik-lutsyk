package com.geekhub.lessons.task2;

interface ChangeSetExtractor<T> {
    ChangeSet extract(T oldObject, T newObject);
}