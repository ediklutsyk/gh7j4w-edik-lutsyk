package com.geekhub.lessons.task2;

import com.geekhub.lessons.task2.license.License;

import java.time.LocalDate;

import static com.geekhub.lessons.task2.license.LicenseType.FULL;

public class Demo {
    public static void main(String[] args) {
        User oldUser = new User(1, "login", "password1", "FullName1");
        oldUser.addLicense(new License(FULL,
                LocalDate.of(2017, 12, 17),
                LocalDate.of(2017, 12, 25)));
        User newUser = new User(1, "login", "password2", "FullName2");
        ChangeSetExtractor extractor = new ChangeSetExtractorImp();

        try {
            ChangeSet changeSet = extractor.extract(oldUser, newUser);
            System.out.println(changeSet.getNewValues());
            System.out.println(changeSet.getOldValues());
            System.out.println(changeSet.toString());
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }


    }
}
