package com.geekhub.lessons.task2;

import com.geekhub.lessons.task2.annotations.Ignore;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ChangeSetExtractorImp implements ChangeSetExtractor<User> {

    @Override
    public ChangeSet extract(User oldObject, User newObject) {
        Field[] oldDeclaredFields = oldObject.getClass().getDeclaredFields();
        Field[] newDeclaredFields = newObject.getClass().getDeclaredFields();
        List<Object> oldValues = new ArrayList<>();
        List<Object> newValues = new ArrayList<>();
        ChangeSet changeSet = new ChangeSet();
        for (int field = 0; field < oldDeclaredFields.length; field++) {
            try {
                oldDeclaredFields[field].setAccessible(true);
                newDeclaredFields[field].setAccessible(true);
                oldValues.add(oldDeclaredFields[field].get(oldObject));
                newValues.add(newDeclaredFields[field].get(newObject));
                boolean isIgnorePresent = oldDeclaredFields[field].isAnnotationPresent(Ignore.class);
                boolean isFieldsEqual = oldDeclaredFields[field].get(oldObject).equals(newDeclaredFields[field].get(newObject));
                if (!isIgnorePresent && !isFieldsEqual) {
                    changeSet.add(oldDeclaredFields[field].get(oldObject));
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Some exceptions falls during extractions. Termination program");
            }
            changeSet.setNewValues(newValues);
            changeSet.setOldValues(oldValues);

        }
        return changeSet;
    }
}
