package com.geekhub.lessons.task2.license;

import java.time.LocalDate;
import java.util.Objects;

public class License {
    private LicenseType type;
    private final LocalDate startDate;
    private final LocalDate expirationDate;


    public License(LicenseType type, LocalDate startDate, LocalDate expirationDate) {
        Objects.requireNonNull(startDate);
        Objects.requireNonNull(expirationDate);
        this.type = type;
        this.startDate = startDate;
        this.expirationDate = expirationDate;
    }

    public LicenseType getType() {
        return type;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    @Override
    public String toString() {
        return "License{" +
                "type=" + type +
                ", startDate=" + startDate +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
