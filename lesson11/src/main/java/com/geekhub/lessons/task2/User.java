package com.geekhub.lessons.task2;

import com.geekhub.lessons.task2.annotations.Ignore;
import com.geekhub.lessons.task2.license.License;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {
    private int id;
    private String login;
    @Ignore
    private String password;
    private String fullName;
    @Ignore
    private boolean admin;
    private List<License> licenses;

    public User(int id, String login, String password, String fullName) {
        this.id = id;
        this.login = Objects.requireNonNull(login);
        this.password = password;
        this.fullName = Objects.requireNonNull(fullName);
        this.licenses = new ArrayList<>();
        this.admin = false;
    }

    public User() {
    }


    @Override
    public String toString() {
        return "User{" +
                "Full name='" + fullName +
                ", login='" + login + '\'' +
                ", Licenses{" + getLicenses() +
                '}';
    }

    int getId() {
        return id;
    }

    String getLogin() {
        return login;
    }

    String getPassword() {
        return password;
    }

    String getFullName() {
        return fullName;
    }

    boolean isAdmin() {
        return admin;
    }

    void setAdmin() {
        this.admin = true;
    }

    void addLicense(License license) {
        licenses.add(license);
    }

    private List<License> getLicenses() {
        return new ArrayList<>(licenses);
    }
}