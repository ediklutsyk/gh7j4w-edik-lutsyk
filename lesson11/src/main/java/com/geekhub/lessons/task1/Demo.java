package com.geekhub.lessons.task1;

import com.geekhub.lessons.task1.exceptions.CanNotCloneException;
import com.geekhub.lessons.task1.implementations.CloneCreatorImp;
import com.geekhub.lessons.task1.interfaces.CanBeCloned;
import com.geekhub.lessons.task1.interfaces.CloneCreator;
import com.geekhub.lessons.task1.license.License;

import java.time.LocalDate;

import static com.geekhub.lessons.task1.license.LicenseType.FULL;

public class Demo {
    public static void main(String[] args) {
        User user = new User(1, "login1", "password1", "FullName1");
        user.addLicense(new License(FULL,
                LocalDate.of(2017, 12, 17),
                LocalDate.of(2017, 12, 25)));
        User user2 = new User(2, "login2", "password2", "FullName2");
        CloneCreator cloneCreator = new CloneCreatorImp();
        CanBeCloned clonedUser = null;
        try {
            clonedUser = cloneCreator.clone(user);
        } catch (CanNotCloneException e) {
            e.printStackTrace();
        }
        System.out.println(clonedUser.equals(user));
        System.out.println(user.equals(user2));
    }
}
