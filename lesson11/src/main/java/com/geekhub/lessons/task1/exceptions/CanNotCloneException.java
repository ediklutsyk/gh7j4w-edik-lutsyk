package com.geekhub.lessons.task1.exceptions;

public class CanNotCloneException extends Exception {
    public CanNotCloneException(String message) {
        super(message);
    }
}
