package com.geekhub.lessons.task1.interfaces;

import com.geekhub.lessons.task1.exceptions.CanNotCloneException;

public interface CloneCreator<T extends CanBeCloned> {
    T clone(T object) throws CanNotCloneException;
}
