package com.geekhub.lessons.task1.license;

public enum LicenseType {
    FULL, WEB_ONLY, MOBILE_ONLY
}
