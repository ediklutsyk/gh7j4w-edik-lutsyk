package com.geekhub.lessons.task1.implementations;

import com.geekhub.lessons.task1.User;
import com.geekhub.lessons.task1.annotations.Ignore;
import com.geekhub.lessons.task1.exceptions.CanNotCloneException;
import com.geekhub.lessons.task1.interfaces.CanBeCloned;
import com.geekhub.lessons.task1.interfaces.CloneCreator;

import java.lang.reflect.Field;

public class CloneCreatorImp implements CloneCreator {

    @Override
    public CanBeCloned clone(CanBeCloned object) throws CanNotCloneException {
        Class<User> objectClass = User.class;
        Field[] declaredFields = objectClass.getDeclaredFields();
        User user = null;
        try {
            user = objectClass.newInstance();
            for (Field field : declaredFields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    try {
                        field.setAccessible(true);
                        field.set(user, field.get(object));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (InstantiationException | IllegalAccessException e) {
            throw new CanNotCloneException("Some exceptions falls during cloning");
        }
        return user;
    }
}
