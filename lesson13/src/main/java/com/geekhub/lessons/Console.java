package com.geekhub.lessons;

import java.util.Scanner;

public class Console {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter host:");
        String host = in.next();
        System.out.println("Enter login:");
        String login = in.next();
        System.out.println("Enter password:");
        String password = in.next();
        System.out.println("Enter data base name:");
        String dbName = in.next();
        DBManager managerDB = new DBManager(host, login, password, dbName);
        try {
            managerDB.createDB();
            processUserInput(in, managerDB);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void processUserInput(Scanner in, DBManager managerDB) {
        String userInput;
        userInput = in.nextLine();
        while (true) {
            System.out.println("Enter SQL query:");
            userInput = in.nextLine();
            if (userInput.equalsIgnoreCase("exit")) {
                break;
            }
            managerDB.executeSQL(userInput);
        }
    }
}

