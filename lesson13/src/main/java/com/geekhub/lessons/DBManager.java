package com.geekhub.lessons;

import java.sql.*;
import java.util.Objects;

public class DBManager {
    private String host;
    private String login;
    private String password;
    private String dbName;

    public DBManager(String host, String login, String password, String dbName) {
        this.host = host;
        this.login = login;
        this.password = password;
        this.dbName = dbName.toLowerCase();
    }

    public void createDB() {
        try (Connection connection = DriverManager.getConnection(host + "/postgres", login, password);
             Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery("SELECT * FROM pg_database WHERE datname = '" + dbName + "'");
            if (!rs.next()) {
                statement.execute("CREATE DATABASE " + dbName + "");
            }
            createTestTable();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTestTable() {
        try (Connection connection = DriverManager.getConnection(host + "/" + dbName, login, password);
             Statement statement = connection.createStatement();
        ) {
            statement.execute("DROP TABLE IF EXISTS SUPERHERO");
            statement.execute("CREATE TABLE Superhero(\n" +
                    "personID SMALLSERIAL PRIMARY KEY,\n" +
                    "personFirstName VARCHAR (50),\n" +
                    "personLastName VARCHAR (50),\n" +
                    "superHeroName VARCHAR (50),\n" +
                    "universe VARCHAR (6) );");

            fillTestTable(statement);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void fillTestTable(Statement statement) throws SQLException {
        statement.execute("INSERT INTO Superhero(personFirstName,personLastName,superHeroName,universe)" +
                " VALUES ('Clark','Kent','Superman','DC')");
        statement.execute("INSERT INTO Superhero(personFirstName,personLastName,superHeroName,universe)" +
                " VALUES ('Bruce','Wayne','Batman','DC')");
        statement.execute("INSERT INTO Superhero(personFirstName,personLastName,superHeroName,universe)" +
                " VALUES ('Peter','Parker','Spider-man','Marvel')");
        statement.execute("INSERT INTO Superhero(personFirstName,personLastName,superHeroName,universe)" +
                " VALUES ('Tony','Stark','Iron man','Marvel')");
        statement.execute("INSERT INTO Superhero(personFirstName,personLastName,superHeroName,universe)" +
                " VALUES ('Bruce','Banner','Hulk','Marvel')");
    }

    public void executeSQL(String sql) {
        try (Connection connection = DriverManager.getConnection(host + "/" + dbName, login, password);
             Statement statement = connection.createStatement();
        ) {
            boolean isResultSet = statement.execute(Objects.requireNonNull(sql));
            if (isResultSet) {
                ResultSet resultSet = statement.getResultSet();
                int numberOfCol = resultSet.getMetaData().getColumnCount();
                while (resultSet.next()) {
                    for (int i = 1; i <= numberOfCol; i++) {
                        System.out.printf("%10s|", String.valueOf(resultSet.getObject(i)));
                    }
                    System.out.print("\n");
                }
            } else {
                int result = statement.getUpdateCount();
                System.out.println("Updated:" + result);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
