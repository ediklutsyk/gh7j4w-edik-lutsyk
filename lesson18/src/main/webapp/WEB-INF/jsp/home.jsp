<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <title>Hello, world!</title>
</head>
<body>
<div class="container">
    </br>
    <form id="form" action="/session" method="get">
        <div class="form-group row">
            <label class="col-1">Action:</label>
            <div class="col-4">
                <select id="select" name="action" class="form-control" required>
                    <option value=""></option>
                    <option value="add">Add</option>
                    <option value="update">Update</option>
                    <option value="invalidate">Invalidate</option>
                    <option value="delete">Remove</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-1">Name:</label>
            <div class="col-8">
                <input id="inputName" class="form-control" type="text" name="name" placeholder="Enter name" required>
            </div>

        </div>
        <div class="form-group row">
            <label class="col-1">Value:</label>
            <div class="col-8">
                <input id="inputValue" class="form-control " type="text" name="value" placeholder="Enter value"
                       required>
            </div>

        </div>
        <input id="submit" class="btn btn-outline-primary" type="submit" value="Submit">
    </form>
    <hr>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Value</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${usersList}" var="usersList" varStatus="loop">
            <tr>
                <th>${loop.count}</th>
                <th> ${usersList.name}</th>
                <th> ${usersList.value}</th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
        integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
        crossorigin="anonymous"></script>
</body>
</html>
