package com.geekhub.lessons.exception;

public class NoUserException extends Exception {
    public NoUserException(String message) {
        super(message);
    }
}
