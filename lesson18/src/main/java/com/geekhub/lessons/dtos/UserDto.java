package com.geekhub.lessons.dtos;

public class UserDto {
    private String name;
    private String value;

    public UserDto(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public UserDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
