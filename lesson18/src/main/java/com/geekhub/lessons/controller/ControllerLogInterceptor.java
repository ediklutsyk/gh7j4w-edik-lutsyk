package com.geekhub.lessons.controller;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class ControllerLogInterceptor extends HandlerInterceptorAdapter {
    private long start;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            this.start = System.currentTimeMillis();
            System.out.println("[" + LocalDateTime.now() + "]: ["
                    + request.getQueryString() + "]   -   START:    "
                    + request.getMethod() + " " + request.getRequestURI());
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long elapsedTime = System.currentTimeMillis() - start;
        if (handler instanceof HandlerMethod) {
            System.out.println("[" + LocalDateTime.now() + "]: [Method execution time: " + elapsedTime
                    + " milliseconds.]   -   COMPLETE: "
                    + request.getMethod() + " " + request.getRequestURI());

        }
    }
}