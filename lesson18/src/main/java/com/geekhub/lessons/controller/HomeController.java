package com.geekhub.lessons.controller;

import com.geekhub.lessons.dao.DaoImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/")
public class HomeController {
    private DaoImpl dao;

    public HomeController(DaoImpl dao) {
        this.dao = dao;
    }

    @GetMapping(value = "home")
    public String byStringViewName() {
        return "home";
    }

    @GetMapping(value = "session")
    public String toSession(@RequestParam("action") String action,
                            @RequestParam("name") String name,
                            @RequestParam("value") String value,
                            Model model) {
        switch (action) {
            case "add":
                dao.add(name, value);
                break;
            case "update":
                dao.update(name, value);
                break;
            case "delete":
                dao.remove(name, value);
                break;
            case "invalidate":
                dao.invalidate(name, value);
                break;
        }
        model.addAttribute("usersList", dao.getUsers());
        return "home";
    }

}