package com.geekhub.lessons.controller;

import com.geekhub.lessons.dao.DaoImpl;
import com.geekhub.lessons.dtos.UserDto;
import com.geekhub.lessons.exception.NoUserException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Rest {
    private DaoImpl dao;

    public Rest(DaoImpl dao) {
        this.dao = dao;
    }

    @GetMapping("user")
    public UserDto showUser() throws NoUserException {
        return dao.getUsers().stream().findAny().orElseThrow(() -> new NoUserException("No users in db"));
    }

    @GetMapping("userList")
    public List<UserDto> showUsers() {
        return dao.getUsers();
    }

    @RequestMapping(value = "exception")
    public void exception() {
        throw new RuntimeException("Some custom error");
    }
}
