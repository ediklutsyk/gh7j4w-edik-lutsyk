package com.geekhub.lessons.controller;

import com.geekhub.lessons.exception.NoUserException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(
        basePackages = {"com.geekhub.lessons"},
        assignableTypes = {HomeController.class}
)
public class ControllerExceptionHandler {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({RuntimeException.class, NoUserException.class})
    public ModelAndView exceptionHandler(Exception ex) {
        ModelAndView error = new ModelAndView("error");
        error.addObject("errorMessage", "Some exception happens: " + ex.toString());
        return error;
    }
}