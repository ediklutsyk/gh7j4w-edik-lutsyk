package com.geekhub.lessons.dao;

import com.geekhub.lessons.dtos.UserDto;

import java.util.List;

public interface Dao {
    List<UserDto> getUsers();

    void add(String name, String value);

    void update(String name, String value);

    void remove(String name, String value);

    void invalidate(String name, String value);
}
