package com.geekhub.lessons.dao;

import com.geekhub.lessons.dtos.UserDto;
import com.geekhub.lessons.config.DbConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DaoImpl implements Dao {
    private DbConfig dbConfig;

    @Autowired
    public DaoImpl(DbConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    @Override
    public List<UserDto> getUsers() {
        List<UserDto> userList = new ArrayList<>();
        try (Connection connection = dbConfig.dataSource().getConnection();
             Statement statement = connection.createStatement();
        ) {
            ResultSet rs = statement.executeQuery("SELECT * FROM namevalue");
            while (rs.next()) {
                UserDto user = new UserDto(
                        rs.getString("name"),
                        rs.getString("value"));
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException("An exception during getting data from DB");
        }
        return userList;
    }

    @Override
    public void add(String name, String value) {
        try (Connection connection = dbConfig.dataSource().getConnection();
             Statement statement = connection.createStatement();
        ) {
            statement.execute("INSERT INTO nameValue(name,value) VALUES ('" + name + "', '" + value + "'); ");
        } catch (SQLException e) {
            throw new RuntimeException("An exception during adding to DB");
        }


    }

    @Override
    public void update(String name, String value) {
        try (Connection connection = dbConfig.dataSource().getConnection();
             Statement statement = connection.createStatement();
        ) {
            statement.execute("UPDATE nameValue SET value='" + value + "' WHERE name='" + name + "';");
        } catch (SQLException e) {
            throw new RuntimeException("An exception during updating to DB");
        }
    }

    @Override
    public void remove(String name, String value) {
        try (Connection connection = dbConfig.dataSource().getConnection();
             Statement statement = connection.createStatement();
        ) {
            statement.execute("DELETE FROM nameValue WHERE name='" + name + "'; ");
        } catch (SQLException e) {
            throw new RuntimeException("An exception during deleting to DB");
        }
    }

    @Override
    public void invalidate(String name, String value) {
        try (Connection connection = dbConfig.dataSource().getConnection();
             Statement statement = connection.createStatement();
        ) {
            statement.execute("DELETE FROM nameValue; ");
        } catch (SQLException e) {
            throw new RuntimeException("An exception during deleting to DB");
        }
    }
}
