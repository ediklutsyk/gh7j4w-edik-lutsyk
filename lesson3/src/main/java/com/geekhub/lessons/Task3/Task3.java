package com.geekhub.lessons.Task3;

public class Task3 {
    public static void main(String[] args) {
        int processors = Runtime.getRuntime().availableProcessors();
        long freeMemory = Runtime.getRuntime().freeMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("Number of processors available to the Java virtual machine: " + processors +
                "\nFree Memory in the Java Virtual Machine(in Bytes): " + freeMemory +
                "\nTotal amount of memory in the Java virtual machine(in Bytes): " + totalMemory +
                "\nMaximum amount of memory that the Java virtual machine will attempt to use(in Bytes): " + maxMemory);
    }
}
