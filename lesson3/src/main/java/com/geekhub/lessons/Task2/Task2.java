package com.geekhub.lessons.Task2;

public class Task2 {
    private static final int NUMBER_OF_ITERATIONS = 10000000;

    public static void main(String[] arg) {
        long startTime = System.currentTimeMillis();
        testTimeBuilder(new StringBuilder("test"));
        long endTime = System.currentTimeMillis();
        System.out.println("StringBuilder took " + (endTime - startTime) + " milliseconds");

        startTime = System.currentTimeMillis();
        testTimeBuffer(new StringBuffer("test"));
        endTime = System.currentTimeMillis();
        System.out.println("StringBuffer took " + (endTime - startTime) + " milliseconds");

        startTime = System.currentTimeMillis();
        testTimeString("test");
        endTime = System.currentTimeMillis();
        System.out.println("String took " + (endTime - startTime) + " milliseconds");

    }

    public static void testTimeBuilder(StringBuilder str) {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            str.append("test");  //452 ms
        }
    }

    public static void testTimeBuffer(StringBuffer str) {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            str.append("test");  //839 ms
        }
    }

    public static void testTimeString(String str) {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            str += "test";  //27703 ms (Too much even on 100000 iterations)
        }
    }

}
