package com.geekhub.lessons.Task1;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        System.out.println("Enter you string:");
        Scanner in = new Scanner(System.in);
        String str = in.next();
        if (isUpperCase(str.charAt(0))) {
            System.out.println(str.toUpperCase());
        } else {
            System.out.println(str.toLowerCase());
        }
    }

    private static boolean isUpperCase(char firstChar) {
        if (firstChar == Character.toUpperCase(firstChar)) {
            return true;
        }
        return false;
    }
}

