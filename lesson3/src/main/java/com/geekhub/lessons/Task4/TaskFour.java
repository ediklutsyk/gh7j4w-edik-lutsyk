package com.geekhub.lessons.Task4;

import static java.lang.Math.floor;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class TaskFour {
    public static void main(String[] args) {
        rootsOfPolynom(1, 1, 3);
        rootsOfPolynom(1, 4, 4);
        rootsOfPolynom(1, 1, -6);
    }

    public static void rootsOfPolynom(int a, int b, int c) {
        int x1;
        double d = discriminant(a, b, c);
        if (floor(d) == d) {
            if (d < 0) {
                System.out.println("No solution ");
            } else if (d == 0) {
                x1 = (-b / (2 * a));
                System.out.format("Root = %d" + "\n", x1);
            } else {
                x1 = (int) (-b + sqrt(d)) / (2 * a);
                int x2 = (int) (-b - sqrt(d)) / (2 * a);
                System.out.format("Root 1 = %d" + ";Root 2 = %d", x1, x2);
            }
        } else {
            System.out.println("Discriminant isn't integer ");
        }
    }

    public static double discriminant(int a, int b, int c) {
        return pow(b, 2) - 4 * a * c;
    }
}
