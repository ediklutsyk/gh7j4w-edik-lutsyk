package com.geekhub.lessons.test;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.Assert.*;

public class CheckResultsTest {

    private CheckResults checkResults;

    @BeforeMethod
    public void setUp() {
        checkResults = new CheckResults();
    }

    @Test
    public void testCheckSuccess() {
        Map<Integer, List<Integer>> userResult = new HashMap<>();
        Map<Integer, List<Integer>> correctResults = new HashMap<>();
        userResult.put(1, Arrays.asList(1,2));
        userResult.put(2, Arrays.asList(3,4));
        correctResults.put(1, Arrays.asList(1,4));
        correctResults.put(2, Arrays.asList(3,4));
        int mark = checkResults.check(userResult,correctResults);
        Assert.assertEquals(mark, 3);
    }

}