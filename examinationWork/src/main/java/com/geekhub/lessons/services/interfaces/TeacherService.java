package com.geekhub.lessons.services.interfaces;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Teacher;
import com.geekhub.lessons.db.persistence.Test;
import com.geekhub.lessons.exceptions.NoSuchTestException;

import java.util.List;
import java.util.Optional;

public interface TeacherService {
    Optional<Teacher> findBy(String email);

    List<Teacher> findAll();

    Teacher save(Teacher teacher);

    void update(Teacher teacher);

    void delete(Teacher teacher);

    Test findTestByName(Teacher teacher, String testName) throws NoSuchTestException;

    void setTeacher(Student student, Teacher teacher);

    void createTest(Test test, Teacher teacher);

    void initializeTest(String testName, Integer mark, Integer time, Teacher teacher);
}