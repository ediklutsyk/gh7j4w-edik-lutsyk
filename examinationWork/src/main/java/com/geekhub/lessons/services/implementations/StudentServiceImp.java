package com.geekhub.lessons.services.implementations;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.StudentMarks;
import com.geekhub.lessons.db.persistence.Test;
import com.geekhub.lessons.repositories.StudentRepository;
import com.geekhub.lessons.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;
    private final ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    public StudentServiceImp(StudentRepository studentRepository, ShaPasswordEncoder shaPasswordEncoder) {
        this.studentRepository = studentRepository;
        this.shaPasswordEncoder = shaPasswordEncoder;
    }

    @Override
    public Optional<Student> findBy(String email) {
        return studentRepository.findByEmail(email);
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student save(Student student) {
        student.setPassword(shaPasswordEncoder.encodePassword(student.getPassword(), null));
        student.setActive(1);
        student.setRole("STUDENT");
        return studentRepository.save(student);
    }

    @Override
    public void saveResults(Student student, Test test, Integer mark) {
        StudentMarks studentMarks = new StudentMarks();
        studentMarks.setCorrectAnswers(mark);
        studentMarks.setTest(test);
        studentMarks.setStudent(student);
        student.getStudentMarks().add(studentMarks);
        studentRepository.save(student);
    }

    @Override
    public int getMarkForTest(Test test, Student student) {
        double countAverageOfCorrect = countAverageOfCorrect(student, test.getName());
        double mark = (countAverageOfCorrect / test.getGeneralAmountOfCorrect()) * test.getMaxMark();
        return (int) Math.round(mark);
    }


    private double countAverageOfCorrect(Student student, String testName) {
        int countSumOfCorrect = countSumOfCorrect(student, testName);
        long countAmountOfCorrect = countAmountOfTries(student, testName);
        return (double) countSumOfCorrect / (double) countAmountOfCorrect;
    }

    private long countAmountOfTries(Student student, String testName) {
        return student.getStudentMarks().stream()
                .filter(marks -> marks.getTest().getName().equals(testName))
                .map(StudentMarks::getCorrectAnswers).count();
    }

    private int countSumOfCorrect(Student student, String testName) {
        return student.getStudentMarks().stream()
                .filter(marks -> marks.getTest().getName().equals(testName))
                .map(StudentMarks::getCorrectAnswers).mapToInt(Integer::intValue).sum();
    }

    @Override
    public void delete(Student student) {
        studentRepository.delete(student);
    }
}