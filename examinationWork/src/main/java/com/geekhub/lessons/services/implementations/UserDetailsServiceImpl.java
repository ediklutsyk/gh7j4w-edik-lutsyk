package com.geekhub.lessons.services.implementations;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Teacher;
import com.geekhub.lessons.services.interfaces.StudentService;
import com.geekhub.lessons.services.interfaces.TeacherService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final StudentService studentService;
    private final TeacherService teacherService;

    public UserDetailsServiceImpl(StudentService studentService, TeacherService teacherService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Optional<Student> optionalStudent = studentService.findBy(username);
        final Optional<Teacher> optionalTeacher = teacherService.findBy(username);
        if (optionalStudent.isPresent()) {
            final Student student = optionalStudent.get();
            return new org.springframework.security.core.userdetails.User(
                    student.getEmail(), student.getPassword(), Collections.singletonList(new SimpleGrantedAuthority(student.getRole()))
            );
        } else if(optionalTeacher.isPresent()){
            final Teacher teacher = optionalTeacher.get();
            return new org.springframework.security.core.userdetails.User(
                    teacher.getEmail(), teacher.getPassword(), Collections.singletonList(new SimpleGrantedAuthority(teacher.getRole()))
            );
        } else {
            throw new UsernameNotFoundException("Incorrect login or password.");
        }
    }
}