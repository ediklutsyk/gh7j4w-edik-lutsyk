package com.geekhub.lessons.services.interfaces;


import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface StudentService {
    Optional<Student> findBy(String email);

    List<Student> findAll();

    Student save(Student student);

    void saveResults(Student student, Test test, Integer mark);

    int getMarkForTest(Test test, Student student);

    void delete(Student student);
}