package com.geekhub.lessons.services.interfaces;


import com.geekhub.lessons.db.persistence.Classroom;
import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Test;

import java.util.List;
import java.util.Optional;

public interface ClassroomService {
    Optional<Classroom> findBy(String key);

    List<Classroom> findAll();

    List<Classroom> findAllByKeys(List<String> key);

    Classroom save(Classroom classroom);

    void addStudent(Student student, Classroom classroom);

    void addTest(Test test, Classroom classroom);

    void delete(Classroom classroom);
}