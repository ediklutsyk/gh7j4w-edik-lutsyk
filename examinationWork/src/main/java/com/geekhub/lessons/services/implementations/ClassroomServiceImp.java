package com.geekhub.lessons.services.implementations;

import com.geekhub.lessons.db.persistence.Classroom;
import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Test;
import com.geekhub.lessons.repositories.ClassroomRepository;
import com.geekhub.lessons.services.interfaces.ClassroomService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClassroomServiceImp implements ClassroomService {
    private final ClassroomRepository classroomRepository;

    public ClassroomServiceImp(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    @Override
    public Optional<Classroom> findBy(String key) {
        return classroomRepository.findByKey(key);
    }

    @Override
    public List<Classroom> findAll() {
        return classroomRepository.findAll();
    }

    @Override
    public  List<Classroom> findAllByKeys(List<String> key) {
        return classroomRepository.findAll().stream().filter(classroom -> key.contains(classroom.getKey())).collect(Collectors.toList());
    }

    @Override
    public Classroom save(Classroom classroom){
        return classroomRepository.save(classroom);
    }

    @Override
    public void addStudent(Student student, Classroom classroom){
        classroom.getStudents().add(student);
        student.getClassrooms().add(classroom);
        save(classroom);
    }

    @Override
    public void addTest(Test test, Classroom classroom){
        classroom.getTests().add(test);
        test.getClassrooms().add(classroom);
        save(classroom);
    }

    @Override
    public void delete(Classroom classroom) {
        classroomRepository.delete(classroom);
    }
}
