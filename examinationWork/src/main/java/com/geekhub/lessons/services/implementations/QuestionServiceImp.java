package com.geekhub.lessons.services.implementations;

import com.geekhub.lessons.db.persistence.Question;
import com.geekhub.lessons.repositories.QuestionRepository;
import com.geekhub.lessons.services.interfaces.QuestionService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional
@SuppressWarnings("Duplicates")
public class QuestionServiceImp implements QuestionService {
    private Pattern START_WITH_CAPITAL = Pattern.compile("^[A-D)]+");
    private Pattern START_WITH_LETTER = Pattern.compile("^[A-Da-d)]+");
    private Pattern QUESTION_NUMBER = Pattern.compile("^[\\d]+");
    private final QuestionRepository questionRepository;

    public QuestionServiceImp(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Optional<Question> findBy(Integer id) {
        return Optional.ofNullable(questionRepository.findOne(id));
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public Question save(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public void delete(Question question) {
        questionRepository.delete(question);
    }

    @Override
    public List<String> getQuestionAnswersWithoutLetterNumeration(List<String> answerList) {
        List<String> questionAnswersWithoutLetterNumeration = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            String answer = answerList.get(i);
            Matcher m = START_WITH_LETTER.matcher(answer);
            while (m.find()) {
                questionAnswersWithoutLetterNumeration.add(answer.substring(m.end()));
            }
        }
        return questionAnswersWithoutLetterNumeration;
    }

    @Override
    public Integer findNumber(Question question) {
        String task = question.getQuestionTask().stream().findFirst().get();
        Matcher m = QUESTION_NUMBER.matcher(task);
        if (m.find()) {
            return Integer.parseInt(task.substring(0, m.end()));
        }
        return -1;
    }

    @Override
    public List<Integer> findCorrectAnswer(List<String> answerList) {
        List<Integer> correctAnswersList = new ArrayList<>();
        for (String answer : answerList) {
            Matcher m = START_WITH_CAPITAL.matcher(answer);
            if (m.find()) {
                correctAnswersList.add(letterToNum(answer.charAt(0)));
            }
        }
        return correctAnswersList;
    }

    private Integer letterToNum(char ch) {
        return ch - 'A' + 1;
    }


}