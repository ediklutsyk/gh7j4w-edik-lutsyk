package com.geekhub.lessons.services.implementations;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Teacher;
import com.geekhub.lessons.db.persistence.Test;
import com.geekhub.lessons.exceptions.NoSuchTestException;
import com.geekhub.lessons.repositories.TeacherRepository;
import com.geekhub.lessons.services.interfaces.TeacherService;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TeacherServiceImp implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final ShaPasswordEncoder shaPasswordEncoder;

    public TeacherServiceImp(TeacherRepository teacherRepository, ShaPasswordEncoder shaPasswordEncoder) {
        this.teacherRepository = teacherRepository;
        this.shaPasswordEncoder = shaPasswordEncoder;
    }

    @Override
    public Optional<Teacher> findBy(String email) {
        return teacherRepository.findByEmail(email);
    }

    @Override
    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher save(Teacher teacher) {
        teacher.setPassword(shaPasswordEncoder.encodePassword(teacher.getPassword(), null));
        teacher.setActive(1);
        teacher.setRole("TEACHER");
        return teacherRepository.save(teacher);
    }

    @Override
    public void update(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Override
    public void delete(Teacher teacher) {
        teacherRepository.delete(teacher);
    }

    @Override
    public Test findTestByName(Teacher teacher, String testName) throws NoSuchTestException {
        return teacher.getTestSet()
                .stream()
                .filter(t -> t.getName().equals(testName))
                .findFirst()
                .orElseThrow(() -> new NoSuchTestException("There isn't any test with name provided"));
    }

    @Override
    public void setTeacher(Student student, Teacher teacher) {
        teacher.getStudents().add(student);
        teacherRepository.save(teacher);
    }

    @Override
    public void createTest(Test test, Teacher teacher) {
        teacher.getTestSet().add(test);
        test.setTeacher(teacher);
        teacherRepository.save(teacher);
    }

    @Override
    public void initializeTest(String testName, Integer mark, Integer time, Teacher teacher) {
        Test test = new Test();
        test.setName(testName);
        test.setMaxMark(mark);
        test.setTime(time);
        teacher.getTestSet().add(test);
        test.setTeacher(teacher);
        teacherRepository.save(teacher);
    }

}