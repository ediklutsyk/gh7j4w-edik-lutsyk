package com.geekhub.lessons.services.interfaces;

import com.geekhub.lessons.db.persistence.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionService {
    Optional<Question> findBy(Integer id);

    List<Question> findAll();

    Question save(Question question);

    void delete(Question question);

    List<String> getQuestionAnswersWithoutLetterNumeration(List<String> answerList);

    Integer findNumber(Question question);

    List<Integer> findCorrectAnswer(List<String> answerList);
}