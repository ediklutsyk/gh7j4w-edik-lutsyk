package com.geekhub.lessons.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserResult {
    public Map<Integer,List<Integer>> userResult = new HashMap<>();

    public Map<Integer, List<Integer>> getUserResult() {
        return userResult;
    }

    public void setUserResult(Map<Integer, List<Integer>> userResult) {
        this.userResult = userResult;
    }
}
