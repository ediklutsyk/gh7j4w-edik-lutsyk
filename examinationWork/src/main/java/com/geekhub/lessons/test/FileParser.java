package com.geekhub.lessons.test;

import com.geekhub.lessons.db.persistence.Question;
import com.geekhub.lessons.db.persistence.Test;
import com.geekhub.lessons.services.interfaces.QuestionService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileParser {
    private final QuestionService questionService;

    public FileParser(QuestionService questionService) {
        this.questionService = questionService;
    }

    public Test parse(MultipartFile file) {
        Path testFilePath;
        Test test = new Test();
        List<String> readAllLinesList;
        try {
            testFilePath = getPath(file);
            readAllLinesList = Files.readAllLines(testFilePath);
        } catch (IOException e) {
            throw new RuntimeException("Error during parsing file");
        }
        List<String> fileContent = replaceEmptyLineWithSeparator(readAllLinesList);
        concatSeparators(fileContent);

        List<String> taskList = new ArrayList<>();
        List<String> answerList = new ArrayList<>();
        Question question = new Question();
        boolean isTask = true;
        for (String line : fileContent) {
            if (!line.matches("^[\n]+")) {
                if (isTask) {
                    taskList.add(line);
                } else {
                    answerList.add(line);
                }
            }
            if (line.matches("^[\n]{2}$")) {
                isTask = false;
                question.setQuestionTask(taskList);
                taskList = new ArrayList<>();
            } else if (line.matches("^[\n]{3}$")) {
                isTask = true;
                question.setTest(test);
                question.setNumber(questionService.findNumber(question));
                question.setCorrectAnswers(questionService.findCorrectAnswer(answerList));
                question.setQuestionAnswers(questionService.getQuestionAnswersWithoutLetterNumeration(answerList));
                test.getQuestionsList().add(question);
                answerList = new ArrayList<>();
                question = new Question();
            }
        }
        return test;
    }

    private Path getPath(MultipartFile file) throws IOException {
        File convertedFile = new File(file.getOriginalFilename());
        convertedFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convertedFile);
        fos.write(file.getBytes());
        fos.close();
        return convertedFile.toPath();
    }

    private void concatSeparators(List<String> readAllLinesList) {
        for (int i = 1; i < readAllLinesList.size() - 1; i++) {
             if (readAllLinesList.get(i).equals("\n") && readAllLinesList.get(i - 1).equals("\n") && readAllLinesList.get(i + 1).equals("\n")) {
                readAllLinesList.set(i, "\n\n\n");
                readAllLinesList.remove(i - 1);
                readAllLinesList.remove(i);
            } else if (readAllLinesList.get(i).equals("\n") && readAllLinesList.get(i - 1).equals("\n")) {
                readAllLinesList.set(i, "\n\n");
                readAllLinesList.remove(i - 1);
            }
        }
        readAllLinesList.add("\n\n\n");
    }

    private List<String> replaceEmptyLineWithSeparator(List<String> fileContent) {
        return fileContent.stream().map(line -> {
            if (line.isEmpty()) {
                line += "\n";
            }
            return line;
        }).collect(Collectors.toList());
    }
}
