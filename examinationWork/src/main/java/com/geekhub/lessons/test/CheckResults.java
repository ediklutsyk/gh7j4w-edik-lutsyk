package com.geekhub.lessons.test;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class CheckResults {

    public int check(Map<Integer, List<Integer>> userResult, Map<Integer, List<Integer>> correctResults) {
        int correct = 0;
        for (int i = 1; i <= userResult.keySet().size(); i++) {
            List<Integer> userResultList = userResult.get(i);
            userResultList.removeAll(Collections.singleton(null));
            List<Integer> correctResultList = correctResults.get(i);
            for (int j = 0; j < userResultList.size(); j++) {
                if (userResultList.get(j).equals(correctResultList.get(j))) {
                    correct++;
                }
            }
        }
        return correct;
    }

}
