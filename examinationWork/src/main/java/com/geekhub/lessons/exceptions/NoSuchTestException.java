package com.geekhub.lessons.exceptions;

public class NoSuchTestException extends Exception {
    public NoSuchTestException(String message) {
        super(message);
    }
}

