package com.geekhub.lessons.exceptions;

public class NoSuchClassroomException extends Exception {
    public NoSuchClassroomException(String message) {
        super(message);
    }
}

