package com.geekhub.lessons.db.persistence;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@Table(name = "question")
public class Question implements Persistable<Integer> {
    private Integer id;
    private Test test;
    private Integer number;
    private List<Integer> correctAnswers;
    private List<String> questionTask;
    private List<String> questionAnswers;

    @Id
    @Override
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    @Transient
    public boolean isNew() {
        return null == getId();
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "testId")
    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    @ElementCollection
    @CollectionTable(name = "Tasks", joinColumns = @JoinColumn(name = "questionId"))
    @Column(name = "tasks")
    public List<String> getQuestionTask() {
        return questionTask;
    }

    public void setQuestionTask(List<String> questionTask) {
        this.questionTask = questionTask;
    }

    @ElementCollection
    @CollectionTable(name = "Answers", joinColumns = @JoinColumn(name = "questionId"))
    @Column(name = "answers")
    public List<String> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<String> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }

    @ElementCollection
    @CollectionTable(name = "correct_answers", joinColumns = @JoinColumn(name = "questionId"))
    @Column(name = "correct_answers")
    public List<Integer> getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(List<Integer> correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

}
