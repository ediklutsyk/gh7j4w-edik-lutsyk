package com.geekhub.lessons.db.persistence;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "test")
public class Test implements Persistable<Integer> {
    private Integer id;
    private String name;
    private Integer maxMark;
    private Integer time;
    private Teacher teacher;
    private Set<Classroom> classrooms = new HashSet<>();
    private List<Question> questionsList = new ArrayList<>();
    private Set<StudentMarks> studentMarks;
    private Integer generalAmountOfCorrect;

    @Id
    @Override
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    @Override
    @Transient
    public boolean isNew() {
        return null == getId();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    public List<Question> getQuestionsList() {
        return questionsList;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacherId")
    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    public Set<StudentMarks> getStudentMarks() {
        return studentMarks;
    }

    public void setStudentMarks(Set<StudentMarks> studentMarks) {
        this.studentMarks = studentMarks;
    }

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "test_classrooms",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "classroom_id")})
    public Set<Classroom> getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(Set<Classroom> classrooms) {
        this.classrooms = classrooms;
    }

    public void setQuestionsList(List<Question> questionsList) {
        this.questionsList = questionsList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxMark() {
        return maxMark;
    }

    public void setMaxMark(Integer maxMark) {
        this.maxMark = maxMark;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getGeneralAmountOfCorrect() {
        return generalAmountOfCorrect;
    }

    public void setGeneralAmountOfCorrect(Integer generalAmountOfCorrect) {
        this.generalAmountOfCorrect = generalAmountOfCorrect;
    }
}
