package com.geekhub.lessons.controllers;

import com.geekhub.lessons.db.persistence.*;
import com.geekhub.lessons.exceptions.NoSuchClassroomException;
import com.geekhub.lessons.exceptions.NoSuchTestException;
import com.geekhub.lessons.exceptions.NoSuchUserException;
import com.geekhub.lessons.services.interfaces.ClassroomService;
import com.geekhub.lessons.services.interfaces.StudentService;
import com.geekhub.lessons.services.interfaces.TeacherService;
import com.geekhub.lessons.test.FileParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/teacher")
public class TeacherController {
    private final StudentService studentService;
    private final TeacherService teacherService;
    private final FileParser fileParser;
    private final ClassroomService classroomService;

    public TeacherController(StudentService studentService,
                             TeacherService teacherService,
                             FileParser fileParser, ClassroomService classroomService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.fileParser = fileParser;
        this.classroomService = classroomService;
    }

    @RequestMapping
    public String index(HttpServletRequest request, Model model) throws NoSuchUserException {
        String username = request.getUserPrincipal().getName();
        Teacher teacher = teacherService.findBy(username)
                .orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        model.addAttribute("teacher", teacher);
        model.addAttribute("newClassroom", new Classroom());
        return "teachersDashboard";
    }

    @RequestMapping(value = "/classroomDetails")
    public String classroomDetails(@RequestParam String classroomKey, Model model) throws NoSuchClassroomException {
        Classroom classroom = classroomService.findBy(classroomKey)
                .orElseThrow(() -> new NoSuchClassroomException("There isn't any classroom with that key"));
        model.addAttribute("classroom", classroom);
        return "teacher_classroom";
    }

    @RequestMapping(value = "/testResults")
    public String testResults(@RequestParam String classroomKey, @RequestParam String testName, Model model) throws NoSuchTestException, NoSuchClassroomException {
        Classroom classroom = classroomService.findBy(classroomKey)
                .orElseThrow(() -> new NoSuchClassroomException("There isn't any classroom with that key"));
        Test test = teacherService.findTestByName(classroom.getTeacher(), testName);
        Map<String, String> testMarks = new HashMap<>();
        classroom.getStudents().forEach(student -> {
            boolean isStudentPassedTest = student.getStudentMarks().stream()
                    .map(StudentMarks::getTest).collect(Collectors.toList()).contains(test);
            if (isStudentPassedTest) {
                testMarks.put(student.getFirstName() + " " + student.getLastName(),
                        studentService.getMarkForTest(test, student) + "/" + test.getMaxMark());
            }
        });
        model.addAttribute("testMarks", testMarks);
        model.addAttribute("classroom", classroom);
        model.addAttribute("test", test);
        return "test_results";
    }

    @RequestMapping(value = "/deleteMe")
    public String deleteTeacher(@RequestParam String username) throws NoSuchUserException {
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        teacherService.delete(teacher);
        return "redirect:/login";
    }

    @RequestMapping("/testCreating")
    public String testCreation(HttpServletRequest request, @RequestParam String type, Model model) throws NoSuchUserException {
        String username = request.getUserPrincipal().getName();
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        model.addAttribute("teacher", teacher);
        model.addAttribute("type", type);
        return "testCreating";
    }

    @RequestMapping("/testCreating/byParsingFile")
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   @RequestParam String username,
                                   @RequestParam String testName,
                                   @RequestParam Integer mark,
                                   @RequestParam Integer time,
                                   @RequestParam(value = "classroomsKeys", required = false) List<String> classroomsKeys,
                                   Model model) throws NoSuchUserException {
        Test test = fileParser.parse(file);
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        test.setName(testName);
        test.setMaxMark(mark);
        test.setTime(time);
        test.setGeneralAmountOfCorrect(test.getQuestionsList().stream()
                .map(question -> question.getCorrectAnswers().size()).mapToInt(Integer::intValue).sum());
        teacherService.createTest(test, teacher);
        model.addAttribute("questions", test.getQuestionsList());
        model.addAttribute("chosenClassrooms", classroomService.findAllByKeys(classroomsKeys));
        model.addAttribute("testName", testName);
        return "testCreating";
    }

    @RequestMapping("/testCreating/byInterface")
    public String testCreatingByInterface(@RequestParam String username,
                                          @RequestParam String testName,
                                          @RequestParam Integer mark,
                                          @RequestParam Integer time,
                                          @RequestParam(value = "classroomsKeys", required = false) List<String> classroomsKeys,
                                          Model model) throws NoSuchUserException {
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        teacherService.initializeTest(testName, mark, time, teacher);
        model.addAttribute("questionNumber", 1);
        model.addAttribute("classrooms", classroomService.findAllByKeys(classroomsKeys));
        model.addAttribute("testName", testName);
        model.addAttribute("question", new Question());
        return "testCreatingByInterface";
    }

    @RequestMapping("/testCreating/addQuestion")
    public String testCreatingByInterface(@Valid Question question,
                                          @RequestParam String username,
                                          @RequestParam String testName,
                                          @RequestParam Integer questionNumber,
                                          @RequestParam(value = "classroomsKeys", required = false) List<String> classroomsKeys,
                                          Model model) throws NoSuchUserException, NoSuchTestException {
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        Test test = teacherService.findTestByName(teacher, testName);
        question.setTest(test);
        question.setNumber(questionNumber);
        test.getQuestionsList().add(question);
        test.setGeneralAmountOfCorrect(test.getQuestionsList().stream()
                .map(q -> q.getCorrectAnswers().size()).mapToInt(Integer::intValue).sum());
        teacherService.update(teacher);
        model.addAttribute("classrooms", classroomService.findAllByKeys(classroomsKeys));
        model.addAttribute("questionNumber", questionNumber + 1);
        model.addAttribute("testName", testName);
        model.addAttribute("question", new Question());
        return "testCreatingByInterface";
    }

    @RequestMapping("/testCreating/deleteTest")
    public String deleteAndQuit(@RequestParam String username, @RequestParam String testName) throws NoSuchUserException, NoSuchTestException {
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        Test test = teacherService.findTestByName(teacher, testName);
        teacher.getTestSet().remove(test);
        teacherService.update(teacher);
        return "redirect:/teacher/testCreating?type=file";
    }

    @RequestMapping("/testCreating/saveAndQuit")
    public String saveAndQuit(@RequestParam(value = "classroomsKeys", required = false) List<String> classroomsKeys,
                              @RequestParam String username, @RequestParam String testName) throws NoSuchUserException, NoSuchTestException {
        Teacher teacher = teacherService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        Test test = teacherService.findTestByName(teacher, testName);
        List<Classroom> classrooms = classroomService.findAllByKeys(classroomsKeys);
        classrooms.forEach(classroom -> classroomService.addTest(test, classroom));
        return "redirect:/teacher/";
    }

    @RequestMapping("/saveClassroom")
    public String saveClassroom(@Valid Classroom classroom, @RequestParam String teacherUsername, Model model) throws NoSuchUserException {
        Optional<Classroom> optionalClassroom = classroomService.findBy(classroom.getKey());
        Teacher teacher = teacherService.findBy(teacherUsername)
                .orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        model.addAttribute("newClassroom", new Classroom());
        model.addAttribute("teacher", teacher);
        if (optionalClassroom.isPresent()) {
            model.addAttribute("error", "There is already a classroom registered with the key provided");
            return "teachersDashboard";
        } else {
            teacher.getClassrooms().add(classroom);
            classroom.setTeacher(teacher);
            teacherService.update(teacher);
            model.addAttribute("successMessage", "Classroom has been created successfully");
        }
        return "teachersDashboard";
    }


}
