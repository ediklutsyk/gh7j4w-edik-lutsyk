package com.geekhub.lessons.controllers;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.services.interfaces.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
public class MainController {
    private final StudentService studentService;

    public MainController(StudentService studentService) {
        this.studentService = studentService;
    }

    @RequestMapping(value = "/home")
    public String home(HttpServletRequest request, Model model) {
        String email = request.getUserPrincipal().getName();
        Optional<Student> optionalStudent = studentService.findBy(email);
        if (optionalStudent.isPresent()){
            return "redirect:/student";
        }
        return "redirect:/teacher";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/access-denied")
    public String accessDenied(Model model) {
        model.addAttribute("errorMessage","Access denied!");
        return "error";
    }


}