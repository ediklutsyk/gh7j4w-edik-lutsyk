package com.geekhub.lessons.controllers;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Teacher;
import com.geekhub.lessons.services.interfaces.StudentService;
import com.geekhub.lessons.services.interfaces.TeacherService;
import com.geekhub.lessons.utils.Validation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/registration")
public class RegistrationController {
    private final StudentService studentService;
    private final TeacherService teacherService;
    private final Validation validation;

    public RegistrationController(StudentService studentService, TeacherService teacherService, Validation validation) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.validation = validation;
    }

    @GetMapping
    public String registration(Model model){
        model.addAttribute("student", new Student());
        model.addAttribute("role","student");
        return "registration";
    }

    @RequestMapping(value = "/chooseRole")
    public String chooseRole(@RequestParam String role, Model model){
        if (role.equalsIgnoreCase("teacher")){
            model.addAttribute("teacher", new Teacher());
            model.addAttribute("role","teacher");
        } else{
            model.addAttribute("student", new Student());
            model.addAttribute("role","student");
        }
        return "registration";
    }

    @RequestMapping(value = "/student")
    public String createNewStudent(@Valid Student student, BindingResult bindingResult, Model model) {
        String email = student.getEmail();
        if (validation.isEmailUnavailable(email)) {
            bindingResult.rejectValue("email", "error.student",
                    "There is already a user registered with the email provided");
        }
        if (!student.getPassword().equals(student.getConfirmPassword())){
            bindingResult.rejectValue("password","error.student", "Passwords should be equal");
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("role","student");
            return "registration";
        } else {
            studentService.save(student);
            model.addAttribute("successMessage", "Student has been registered successfully");
            model.addAttribute("student", new Student());
            model.addAttribute("role","student");
        }
        return "registration";
    }

    @RequestMapping(value = "/teacher")
    public String createNewTeacher(@Valid Teacher teacher, BindingResult bindingResult, Model model) {
        String email = teacher.getEmail();
        if (validation.isEmailUnavailable(email)) {
            bindingResult.rejectValue("email", "error.teacher",
                            "There is already a user registered with the email provided");
        }
        if (!teacher.getPassword().equals(teacher.getConfirmPassword())){
            bindingResult.rejectValue("password","error.student", "Passwords should be equal");
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("role","teacher");
            return "registration";
        } else {
            teacherService.save(teacher);
            model.addAttribute("successMessage", "Teacher has been registered successfully");
            model.addAttribute("student", new Student());
            model.addAttribute("role","student");
        }
        return "registration";
    }
}
