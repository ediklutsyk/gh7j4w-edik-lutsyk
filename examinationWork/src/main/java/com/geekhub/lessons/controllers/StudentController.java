package com.geekhub.lessons.controllers;

import com.geekhub.lessons.db.persistence.*;
import com.geekhub.lessons.exceptions.NoSuchClassroomException;
import com.geekhub.lessons.exceptions.NoSuchTestException;
import com.geekhub.lessons.exceptions.NoSuchUserException;
import com.geekhub.lessons.services.interfaces.ClassroomService;
import com.geekhub.lessons.services.interfaces.StudentService;
import com.geekhub.lessons.services.interfaces.TeacherService;
import com.geekhub.lessons.test.CheckResults;
import com.geekhub.lessons.test.UserResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/student")
public class StudentController {
    private final StudentService studentService;
    private final TeacherService teacherService;
    private final ClassroomService classroomService;
    private final CheckResults checkResults;

    public StudentController(StudentService studentService,
                             TeacherService teacherService,
                             ClassroomService classroomService,
                             CheckResults checkResults) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.classroomService = classroomService;
        this.checkResults = checkResults;
    }

    @RequestMapping
    public String index(HttpServletRequest request, Model model) throws NoSuchUserException {
        String username = request.getUserPrincipal().getName();
        Student student = studentService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any student with that username"));
        model.addAttribute("student", student);
        return "studentsDashboard";
    }

    @RequestMapping(value = "/deleteMe")
    public String deleteStudent(@RequestParam String username) throws NoSuchUserException {
        Student student = studentService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any student with that username"));
        studentService.delete(student);
        return "redirect:/login";
    }

    @RequestMapping(value = "/studentResults")
    public String studentResults(HttpServletRequest request, Model model) throws NoSuchUserException {
        String username = request.getUserPrincipal().getName();
        Student student = studentService.findBy(username).orElseThrow(() -> new NoSuchUserException("There isn't any student with that username"));
        Map<String, String> testMarks = new HashMap<>();
        List<Test> passedTest = student.getStudentMarks().stream().map(StudentMarks::getTest).collect(Collectors.toList());
        passedTest.forEach(test -> testMarks.put(test.getName(), studentService.getMarkForTest(test, student) + "/" + test.getMaxMark()));
        model.addAttribute("testMarks", testMarks);
        model.addAttribute("student", student);
        return "student_results";
    }

    @RequestMapping(value = "/classroomDetails")
    public String classroomDetails(@RequestParam String classroomKey, Model model) throws NoSuchClassroomException {
        Classroom classroom = classroomService.findBy(classroomKey)
                .orElseThrow(() -> new NoSuchClassroomException("There isn't any classroom with that key"));
        model.addAttribute("classroom", classroom);
        return "classroom";
    }

    @RequestMapping(value = "/enterClassroom")
    public String enterClassroom(@RequestParam String key, @RequestParam String username, Model model) throws NoSuchUserException {
        Optional<Classroom> optionalClassroom = classroomService.findBy(key);
        Student student = studentService.findBy(username)
                .orElseThrow(() -> new NoSuchUserException("There isn't any student with that username"));
        if (optionalClassroom.isPresent()) {
            Classroom classroom = optionalClassroom.get();
            Teacher teacher = classroom.getTeacher();
            classroomService.addStudent(student, classroom);
            teacherService.setTeacher(student, teacher);
            model.addAttribute("successMessage",
                    "You enter the classroom " + classroom.getName() + " with teacher "
                            + teacher.getFirstName() + " " + teacher.getLastName());
        } else {
            model.addAttribute("error", "There isn't any classroom with provided key");
        }
        model.addAttribute("student", student);
        return "studentsDashboard";
    }


    @RequestMapping(value = "/chooseTest")
    public String chooseTest(@RequestParam String teachersUsername,
                             @RequestParam String test, Model model) throws NoSuchUserException, NoSuchTestException {
        Teacher teacher = teacherService.findBy(teachersUsername)
                .orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        model.addAttribute("userResult", new UserResult());
        model.addAttribute("test", teacherService.findTestByName(teacher, test));
        return "testWriting";
    }

    @RequestMapping("/submitResults")
    public String checkResults(@Valid UserResult userResult,
                               @RequestParam String teachersUsername,
                               @RequestParam String studentUsername,
                               @RequestParam String testName, Model model) throws NoSuchUserException, NoSuchTestException {
        Map<Integer, List<Integer>> userResultMap = userResult.getUserResult();
        Teacher teacher = teacherService.findBy(teachersUsername)
                .orElseThrow(() -> new NoSuchUserException("There isn't any teacher with that username"));
        Test test = teacherService.findTestByName(teacher, testName);
        Student student = studentService.findBy(studentUsername)
                .orElseThrow(() -> new NoSuchUserException("There isn't any student with that username"));
        Map<Integer, List<Integer>> correctResults = test.getQuestionsList().stream()
                .collect(Collectors.toMap(Question::getNumber, Question::getCorrectAnswers));
        Integer mark = checkResults.check(userResultMap, correctResults);
        studentService.saveResults(student, test, mark);
        model.addAttribute("result", mark);
        model.addAttribute("test", test);
        return "testWriting";
    }


}
