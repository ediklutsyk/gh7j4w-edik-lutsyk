package com.geekhub.lessons.utils;

import com.geekhub.lessons.db.persistence.Student;
import com.geekhub.lessons.db.persistence.Teacher;
import com.geekhub.lessons.services.interfaces.StudentService;
import com.geekhub.lessons.services.interfaces.TeacherService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Validation {
    private final StudentService studentService;
    private final TeacherService teacherService;

    public Validation(StudentService studentService, TeacherService teacherService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
    }

    public boolean isEmailUnavailable(String username){
        Optional<Teacher> teacherExist = teacherService.findBy(username);
        Optional<Student> studentExists = studentService.findBy(username);
        return teacherExist.isPresent() || studentExists.isPresent();
    }
}
