$(document).ready(function(){
    $('.show-menu').click(function (){
        $('.menu').slideToggle("medium");
    });
    $('#test_menu').click(function (){
        $('#test_menu_hidden').slideToggle("medium");
    });
    $("#result").on("hidden.bs.modal", function () {
        window.location.href = "/student";
    });
});

function countdown(time) {
    var timer2 = time;
    var interval = setInterval(function() {
        var timer = timer2.split(':');
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes==0 && seconds==0){
            clearInterval(interval);
            setTimeout(submitTest, 1000);
        }
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        $('.countdown').html(minutes + ':' + seconds);
        timer2 = minutes + ':' + seconds;
    }, 1000);
}

function submitTest(){
    $('#submitResults').submit();
}
