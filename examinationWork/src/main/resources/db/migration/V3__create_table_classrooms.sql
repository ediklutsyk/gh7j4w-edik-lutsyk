create table classroom (
  id integer generated by default as identity,
  key varchar(255),
  name varchar(255),
  teacherId integer,
  primary key (id)
)