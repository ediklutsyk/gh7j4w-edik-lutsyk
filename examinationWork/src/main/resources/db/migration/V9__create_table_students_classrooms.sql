create table students_classrooms (
  student_id integer not null,
  classroom_id integer not null,
  primary key (student_id, classroom_id)
)