create table teaching_students (
  student_id integer not null,
  teacher_id integer not null,
  primary key (student_id, teacher_id)
)