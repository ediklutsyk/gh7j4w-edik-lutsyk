package com.geekhub.lessons;

import com.geekhub.lessons.controllers.ControllerLogInterceptor;
import com.geekhub.lessons.logger.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@SpringBootApplication
@PropertySource("classpath:prop/loggerInput.yml")
public class Application extends WebMvcConfigurationSupport {
    @Autowired
    private LoggerService loggerService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ControllerLogInterceptor(loggerService));
    }
}
