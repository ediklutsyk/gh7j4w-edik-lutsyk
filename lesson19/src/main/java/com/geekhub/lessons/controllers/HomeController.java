package com.geekhub.lessons.controllers;

import com.geekhub.lessons.dao.DaoImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private DaoImp daoImp;


    @RequestMapping("/home")
    public String index(Model model) {
        model.addAttribute("users", daoImp.findAll());
        return "index";
    }

    @GetMapping(value = "session")
    public String toSession(@RequestParam("action") String action,
                            @RequestParam("name") String name,
                            @RequestParam("value") String value,
                            Model model) {
        switch (action) {
            case "add":
                daoImp.add(name, value);
                break;
            case "update":
                daoImp.update(name, value);
                break;
            case "delete":
                daoImp.remove(name, value);
                break;
            case "invalidate":
                daoImp.invalidate(name, value);
                break;
        }
        model.addAttribute("users", daoImp.findAll());
        return "index";
    }

}
