package com.geekhub.lessons.controllers;

import com.geekhub.lessons.logger.LoggerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

@Controller
public class ControllerLogInterceptor extends HandlerInterceptorAdapter {

    private LoggerService loggerService;

    public ControllerLogInterceptor(LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            request.setAttribute("start", System.currentTimeMillis());
            loggerService.print(("[" + LocalDateTime.now() + "]: ["
                    + request.getQueryString() + "]   -   START:    "
                    + request.getMethod() + " " + request.getRequestURI()));
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (handler instanceof HandlerMethod) {
            loggerService.print("[" + LocalDateTime.now() + "]: [Method execution time: " +
                    (System.currentTimeMillis() - (Long) request.getAttribute("start"))
                    + " milliseconds.]   -   COMPLETE: "
                    + request.getMethod() + " " + request.getRequestURI());

        }
    }
}