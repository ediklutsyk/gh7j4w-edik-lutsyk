package com.geekhub.lessons.controllers;

import com.geekhub.lessons.dao.Dao;
import com.geekhub.lessons.dtos.User;
import com.geekhub.lessons.exceptions.NoUserException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Rest {
    private Dao dao;

    public Rest(Dao dao) {
        this.dao = dao;
    }

    @GetMapping("user")
    public User showUser() throws NoUserException {
        return dao.findAll().stream().findAny().orElseThrow(() -> new NoUserException("No users in db"));
    }

    @GetMapping("userList")
    public List<User> showUsers() {
        return dao.findAll();
    }

    @RequestMapping(value = "exception")
    public void exception() {
        throw new RuntimeException("Some custom error");
    }
}
