package com.geekhub.lessons.logger;

public interface LoggerService {
    void print(Object object);
}
