package com.geekhub.lessons.logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Component
public class LoggerServiceImpl implements LoggerService {
    @Value("${type}")
    private String prop;

    @Override
    public void print(Object object) {
        switch (prop) {
            case "console":
                System.out.println(String.valueOf(object));
                break;
            case "file":
                try {

                    Path log = Paths.get("logs.txt");
                    if (Files.exists(log)) {
                        List<String> logList = Files.readAllLines(log);
                        logList.add(String.valueOf(object));
                        Files.write(log, logList);
                    } else {
                        Files.write(log, String.valueOf(object).getBytes());
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Exception during writing logs to file");
                }
                break;
        }


    }
}
