package com.geekhub.lessons.exceptions;

public class NoUserException extends Exception {
    public NoUserException(String message) {
        super(message);
    }
}
