package com.geekhub.lessons.dao;

import com.geekhub.lessons.dtos.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;

@Repository
public class DaoImp implements Dao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<User> findAll() {
        List<User> userList = jdbcTemplate.query("SELECT * FROM users", new UserMapper());
        userList.sort(Comparator.comparing(User::getId));
        return userList;
    }

    public void add(String name, String value) {
        jdbcTemplate.update("INSERT INTO users(name, value) VALUES(?,?);", name, value);
    }

    @Override
    public void update(String name, String value) {
        jdbcTemplate.update("UPDATE users SET value='" + value + "' WHERE name='" + name + "';");
    }

    @Override
    public void remove(String name, String value) {
        jdbcTemplate.update("DELETE FROM users WHERE name='" + name + "';");
    }

    @Override
    public void invalidate(String name, String value) {
        jdbcTemplate.update("DELETE FROM users;");
    }
}
