package com.geekhub.lessons.task3;

import java.util.*;

public class Stack implements Queue {
    private List queue;

    public Stack(List queue) {
        this.queue = queue;
    }

    @Override
    public Object peek() {
        return queue.get(queue.size() - 1);
    }

    @Override
    public Object poll() {
        Object polledObj = queue.get(queue.size() - 1);
        queue.remove(queue.size() - 1);
        return polledObj;
    }

    @Override
    public boolean isEmpty() {
        if (queue.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean add(Object o) {
        queue.add(o);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        queue.remove(o);
        return true;
    }


    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public Object remove() {
        return null;
    }

    @Override
    public Object element() {
        return null;
    }


}
