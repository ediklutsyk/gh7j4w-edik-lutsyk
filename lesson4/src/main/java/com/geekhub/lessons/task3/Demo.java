package com.geekhub.lessons.task3;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List queue = new ArrayList();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        Stack stack = new Stack(queue);

        System.out.println("Using peek:");
        for (int i = 0; i < stack.size(); i++) {
                System.out.println(stack.peek());

      }
        System.out.println("Using poll:");
        while (!stack.isEmpty()) {
            System.out.println(stack.poll());
        }
    }
}
