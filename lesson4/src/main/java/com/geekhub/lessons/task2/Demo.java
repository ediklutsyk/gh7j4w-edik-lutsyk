package com.geekhub.lessons.task2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        VoteBox bulletin = new VoteBox();
        List<String> examplesList = Arrays.asList("Candidate1", "Candidate2", "Candidate3", "Candidate4");
        examplesList.forEach(bulletin::add);
        System.out.println(bulletin);
    }
}
