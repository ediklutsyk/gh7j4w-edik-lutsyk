package com.geekhub.lessons.task2;

import java.util.*;

public class VoteBox implements Collection {
    private String[] voteBox = new String[100];
    private final static String constant1 = "Candidate1";
    private final static String constant2 = "Candidate2";

    @Override
    public String toString() {
        String toString = "VoteBox: ";
        int firstNotNullIndex = firstNotNullIndex(voteBox);
        for (int i = 0; i < firstNotNullIndex; i++) {
            toString += voteBox[i] + " ";
        }
        return toString;
    }

    @Override
    public int size() {
        return voteBox.length;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean contains(Object o) {
        int firstNotNullIndex = firstNotNullIndex(voteBox);
        for (int i = 0; i < firstNotNullIndex; i++) {
            if (voteBox[i].equalsIgnoreCase(String.valueOf(o))) {
                return true;
            }
        }
        return false;
    }

    private int firstNotNullIndex(String[] arr) {
        for (int i = 0; i < arr.length; ++i)
            if (arr[i] == null) {
                return i;
            }
        return arr.length;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        int firstNotNullIndex = firstNotNullIndex(voteBox);
        if (o.equals(constant1)) {
            return false;
        } else if (o.equals(constant2)) {
            voteBox[firstNotNullIndex] = String.valueOf(o);
            voteBox[firstNotNullIndex + 1] = String.valueOf(o);
            return true;
        } else {
            voteBox[firstNotNullIndex] = String.valueOf(o);
            return true;
        }
    }

    @Override
    public boolean remove(Object o) {
        return true;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
