package com.geekhub.lessons.task4;

import java.util.HashSet;
import java.util.Set;

public class Demo {
    public static void main(String[] args) {
        Set firstSet = new HashSet();
        firstSet.add(1);
        firstSet.add(2);
        firstSet.add(3);
        Set secondSet = new HashSet();
        secondSet.add(3);
        secondSet.add(4);
        secondSet.add(5);
        SetResult result = new SetResult();
        System.out.println(result.equals(firstSet, secondSet));
        System.out.println(result.union(firstSet, secondSet));
        System.out.println(result.subtract(firstSet, secondSet));
        System.out.println(result.intersect(firstSet, secondSet));
        System.out.println(result.symmetricSubtract(firstSet, secondSet));
    }
}
