package com.geekhub.lessons.task4;

import java.util.Set;

public interface SetOperations {
    boolean equals(Set A, Set B);

    Set union(Set A, Set B);

    Set subtract(Set A, Set B);

    Set intersect(Set A, Set B);

    Set symmetricSubtract(Set A, Set B);
}
