package com.geekhub.lessons.task4;

import java.util.HashSet;
import java.util.Set;

public class SetResult implements SetOperations {
    @Override
    public boolean equals(Set A, Set B) {
        return A.containsAll(B) && B.containsAll(A);
    }

    @Override
    public Set union(Set A, Set B) {
        Set setAB = new HashSet();
        setAB.addAll(A);
        setAB.addAll(B);
        return setAB;
    }

    @Override
    public Set subtract(Set A, Set B) {
        Set setAB = new HashSet();
        setAB.addAll(A);
        setAB.addAll(B);
        setAB.removeAll(B);
        return setAB;
    }

    @Override
    public Set intersect(Set A, Set B) {
        Set setAB = new HashSet();
        setAB.addAll(A);
        setAB.retainAll(B);
        return setAB;
    }

    @Override
    public Set symmetricSubtract(Set A, Set B) {
        Set setAB = new HashSet();
        setAB.addAll(A);
        setAB.addAll(B);
        setAB.removeAll(intersect(A, B));
        return setAB;
    }
}
