package com.geekhub.lessons.task1;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Demo {
    public static void main(String[] args) {
        Map<Integer, int[]> map = new HashMap<>();

        Cat cat1 = new Cat(1, new int[]{0, 0, 0});
        Cat cat2 = new Cat(2, new int[]{255, 255, 255});
        Cat cat3 = new Cat(3, new int[]{154, 48, 1});
        Cat cat4 = new Cat(4, new int[]{255, 0, 0});
        Cat cat5 = new Cat(5, new int[]{255, 0, 255});
        Cat cat6 = new Cat(6, new int[]{255, 255, 255});

        map.put(cat1.getAge(), cat1.getRgbColor());
        map.put(cat2.getAge(), cat2.getRgbColor());
        map.put(cat3.getAge(), cat3.getRgbColor());
        map.put(cat4.getAge(), cat4.getRgbColor());
        map.put(cat5.getAge(), cat5.getRgbColor());
        map.put(cat6.getAge(), cat6.getRgbColor());


        map.forEach((key, value) -> {
            System.out.println("{" + key + ":" + Arrays.toString(value) + "}");
        });

        System.out.println("\nMap contains key 3:" + map.containsKey(3));
        System.out.println("Map contains key 7:" + map.containsKey(7));


        Collection valuesCollection = map.values();
        int white = 0;
        int black = 0;
        int green = 0;
        System.out.println("\nIs there any white,black or green cat?");
        for (Object item : valuesCollection) {
            if (Arrays.equals((int[]) item, new int[]{255, 255, 255})) {
                white++;
            }
            if (Arrays.equals((int[]) item, new int[]{0, 0, 0})) {
                black++;
            }
            if (Arrays.equals((int[]) item, new int[]{0, 255, 0})) {
                green++;
            }
        }
        if (white != 0) {
            System.out.println("There is " + white + " white cat");
        } else {
            System.out.println("There isn't any white cats");
        }
        if (black != 0) {
            System.out.println("There is " + black + " black cat");
        } else {
            System.out.println("There isn't any black cats");
        }
        if (green != 0) {
            System.out.println("There is " + green + " green cat");
        } else {
            System.out.println("There isn't any green cats");
        }

    }
}
