package com.geekhub.lessons.task1;

import java.util.Arrays;

public class Cat {
    private int age;
    private int[] rgbColor = new int[3];

    public Cat(int age, int[] rgbColor) {
        this.age = age;
        if(colorValidation(rgbColor)){
            this.rgbColor = rgbColor;
        }else {
            throw new IllegalArgumentException("Value of color is invalid");
        }
    }
    private boolean colorValidation(int[] rgbColor){
        for(int i=0;i<rgbColor.length;i++){
            if(rgbColor[i]>256 || rgbColor[i]<0) {
                return false;
            }
        }
        return true;
    }

    public int getAge() {
        return age;
    }

    public int[] getRgbColor() {
        return rgbColor;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "age=" + age +
                ", rgbColor=" + Arrays.toString(rgbColor) +
                '}';
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + Arrays.hashCode(rgbColor);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o==null || getClass() != o.getClass()){
            return false;
        }

        Cat cat = (Cat) o;

        if (age != cat.age) return false;
        return Arrays.equals(rgbColor, cat.rgbColor);
    }

}
