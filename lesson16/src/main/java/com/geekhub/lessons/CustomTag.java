package com.geekhub.lessons;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CustomTag extends SimpleTagSupport {

    private Map<String, Set<String>> nameValueMap = new HashMap<>();

    public void setMap(Map<String, Set<String>> nameValueMap) {
        this.nameValueMap = nameValueMap;
    }

    @Override
    public void doTag() {
        nameValueMap.forEach((k, v) -> {
            Object[] arr = v.toArray();
            for (int i = 0; i < arr.length; i++) {
                try {
                    if (i == 0) {
                        printRow(k, arr[i], true);

                    } else {
                        printRow(k, arr[i], false);
                    }
                } catch (IOException e) {
                    System.out.println(e.getClass() + ":" + e.getMessage());
                }
            }

        });
    }

    private void printRow(String name, Object value, boolean isFirst) throws IOException {
        String shownName;
        if (isFirst) {
            shownName = name;
        } else {
            shownName = "";
        }
        getJspContext().getOut().write("" +
                "<tr>\n" +
                "   <td>" + shownName + "</td>\n" +
                "   <td>" + value + "</td>\n" +
                "   <td><a href=\"/session?name=" + name + "&value=" + value + "&action=delete\">Delete</a></td>\n" +
                "</tr>");
    }
}
