package com.geekhub.lessons;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@WebServlet(urlPatterns = "/session", name = "session")
public class Session extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter("action")) {
            case "add":
                add(req, resp);
                break;
            case "delete":
                delete(req, resp);
                break;
        }
    }

    private Map<String, Set<String>> getUsersMapFromSession(HttpServletRequest req) {
        Map<String, Set<String>> users;
        HttpSession session = req.getSession();
        if (session == null || session.getAttribute("nameValueMap") == null) {
            return users = new HashMap<>();
        } else {
            return users = (Map<String, Set<String>>) session.getAttribute("nameValueMap");
        }
    }

    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Map<String, Set<String>> nameValueMap = getUsersMapFromSession(req);
        String name = req.getParameter("name");
        nameValueMap.get(name).remove(req.getParameter("value"));
        if (nameValueMap.get(name).isEmpty()) {
            nameValueMap.remove(name);
        }
        session.setAttribute("nameValueMap", nameValueMap);
        req.getRequestDispatcher("/WEB-INF/pages/session.jsp").forward(req, resp);
    }

    public void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Map<String, Set<String>> nameValueMap = getUsersMapFromSession(req);
        Set<String> valueSet = new HashSet<>();
        String name = req.getParameter("name");
        String value = req.getParameter("value");
        if (nameValueMap.keySet().contains(name)) {
            nameValueMap.get(name).add(value);
        } else {
            valueSet.add(value);
            nameValueMap.put(name, valueSet);
        }
        session.setAttribute("nameValueMap", nameValueMap);
        req.getRequestDispatcher("/WEB-INF/pages/session.jsp").forward(req, resp);

    }

}
