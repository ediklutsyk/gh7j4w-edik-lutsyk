package com.geekhub.lessons;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/home", name = "home")
public class HomeServlet extends HttpServlet {
    protected void doGet(
            HttpServletRequest req, HttpServletResponse resp
    ) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/pages/session.jsp").forward(req, resp);
    }
}
