<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="custom" uri="/WEB-INF/tld/CustomTagDescription.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <title>Session</title>
</head>
<body>
<br>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Value</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${not empty nameValueMap}">
                        <custom:CustomTag map="${nameValueMap}"/>
                    </c:when>
                    <c:otherwise>
                        <tr></tr>
                    </c:otherwise>
                </c:choose>
                <tr>
                    <form action="/session">
                        <td><input class="form-control" type="text" name="name" placeholder="Enter name" required></td>
                        <td><input class="form-control" type="text" name="value" placeholder="Enter value" required>
                        </td>
                        <th>
                            <button class="btn btn-outline-primary btn-block" name="action" value="add">Add</button>
                        </th>
                    </form>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>
