package com.geekhub.lessons.task1;

import geek.User;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Main {
    public static void main(String[] args) {
        try {
            Path tempDir = Files.createTempDirectory("tmp");
            Path pathUsers = downloadingAndUnpackingZip(tempDir);

            List<User> listUsers = FileUtils.getUsers(pathUsers.toFile());
            int allUsersCount = listUsers.size();

            String mostFavoriteEncodingAmongAllUsers = findTheMostFavoriteEncoding(listUsers);

            double averageAccessLevel = listUsers.stream()
                    .mapToInt(User::getAccessLevel).summaryStatistics().getAverage();

            int userWithLongestName = listUsers.stream()
                    .filter(user -> user.getAccessLevel() > averageAccessLevel)
                    .sorted((u1, u2) -> Integer.compare(u2.getName().length(), u1.getName().length()))
                    .findFirst().get().getId();

            Path file1 = tempDir.resolve("file1.txt");
            Path file2 = tempDir.resolve("file2.txt");
            String encodingType1 = mostFavoriteEncodingAmongAllUsers + "-" + userWithLongestName;
            String encodingType2 = mostFavoriteEncodingAmongAllUsers + "-" + allUsersCount;
            List<String> usersNames = new ArrayList<>();
            listUsers.forEach(user -> usersNames.add(user.getName()));
            FileUtils.writeUsers(usersNames, file1, encodingType1);
            FileUtils.writeUsers(usersNames, file2, encodingType2);
            System.out.println(Files.size(file2) - Files.size(file1));

        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private static String findTheMostFavoriteEncoding(List<User> listUsers) {
        Set<String> encodingSet = new HashSet<>();
        listUsers.forEach(user -> encodingSet.add(user.getFavoriteEncoding()));
        Map<String, Long> encodingMap = new HashMap<>();
        encodingSet.forEach(encoding -> {
            long amountOfUsersForEncoding = listUsers.stream().filter(user -> user.getFavoriteEncoding().equals(encoding)).count();
            encodingMap.put(encoding, amountOfUsersForEncoding);
        });

        return encodingMap.entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .findFirst().get().getKey();
    }

    private static Path downloadingAndUnpackingZip(Path tempDir) throws IOException {
        Path downloadDir = Paths.get("D:\\Download\\users.zip");
        Path tempUsersZip = tempDir.resolve("users.zip");
        Files.copy(downloadDir, tempUsersZip);

        //ZipUtils.downloadZip("https://drive.google.com/open?id=1LL5i2ak80phtDudooD0gC0QCVljUH-wQ", tempUsersZip);

        ZipUtils.unpackZip(String.valueOf(tempDir), "/users.zip");

        return Files.walk(tempDir).filter(path -> path.getFileName().toString().equals("users.dat"))
                .findFirst().orElseThrow(NoSuchElementException::new);
    }


}


