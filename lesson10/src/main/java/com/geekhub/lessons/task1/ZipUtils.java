package com.geekhub.lessons.task1;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtils {
    public static boolean unpackZip(String path, String zipname) {

        try (InputStream is = new FileInputStream(path + zipname);
             ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is))
        ) {
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;

                String filename = ze.getName();
                if (ze.isDirectory()) {
                    Path directPath = Paths.get(path + zipname.substring(0, zipname.length() - 4) + "\\" + filename);
                    Files.createDirectories(directPath);
                } else {
                    Path directPath = Paths.get(path + zipname.substring(0, zipname.length() - 4) + "\\");
                    Files.createDirectories(directPath);
                    FileOutputStream fout = new FileOutputStream(path + zipname.substring(0, zipname.length() - 4) + "\\" + filename);

                    while ((count = zis.read(buffer)) != -1) {
                        baos.write(buffer, 0, count);
                        byte[] bytes = baos.toByteArray();
                        fout.write(bytes);
                        baos.reset();
                    }

                    fout.close();
                    zis.closeEntry();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    static void downloadZip(String url, Path destination) {
        URL source = null;
        try {
            source = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try (
                InputStream in = source.openStream();
                OutputStream out = Files.newOutputStream(destination)
        ) {
            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
