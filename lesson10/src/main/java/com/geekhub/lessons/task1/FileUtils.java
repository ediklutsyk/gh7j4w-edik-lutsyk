package com.geekhub.lessons.task1;

import geek.User;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static void writeUsers(List<String> usersNames, Path dataFile, String encodingType) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(dataFile, Charset.forName(encodingType))) {
            for (String name : usersNames) {
                writer.write(name);
                writer.newLine();
            }
        }
    }


    public static List<User> getUsers(File dataFile) throws IOException, ClassNotFoundException {
        List<User> users = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(dataFile);
             BufferedInputStream bis = new BufferedInputStream(fis);
             ObjectInputStream in = new ObjectInputStream(bis)) {
            while (true) {
                Object object = in.readObject();
                if (object instanceof User) {
                    users.add((User) object);
                }
            }
        } catch (EOFException e) {
        }
        return users;
    }
}
