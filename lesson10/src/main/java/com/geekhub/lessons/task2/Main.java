package com.geekhub.lessons.task2;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter directory to traverse: ");
        String workingDir = in.next();
        Path workingDirPath = Paths.get(workingDir);
        String userInput;
        do {
            System.out.println("Press 1 to create new directory\n" +
                    "Press 2 to rename the folder with the specified name\n" +
                    "Press 3 to delete the folder with the specific name\n" +
                    "Enter 'exit' to quit program");
            userInput = in.next();
            switch (userInput) {
                case "1":
                    if(!createNewDirectory(in, workingDirPath)){
                        System.out.println("Something went wrong. Terminating program...");
                        userInput= "exit";
                    }
                    break;
                case "2":
                    if(!renameDirectory(in, workingDirPath)){
                        System.out.println("Something went wrong. Terminating program...");
                        userInput= "exit";
                    }
                    break;
                case "3":
                    if(!deleteDirectory(in, workingDirPath)){
                        System.out.println("Something went wrong. Terminating program...");
                        userInput= "exit";
                    }
                    break;
            }
        } while (!userInput.equalsIgnoreCase("exit"));
    }

    private static boolean deleteDirectory(Scanner in, Path workingDirPath) {
        try {
            System.out.println("Enter folder name:");
            String folderName = in.next();
            Path deleteFolder = workingDirPath.resolve(folderName);
            if (cleaningDirectory(in, deleteFolder)){
                return true;
            }
            Files.deleteIfExists(deleteFolder);
        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }


    private static boolean renameDirectory(Scanner in, Path workingDirPath) {
        try {
            System.out.println("Enter old folder name or path:");
            String oldFolderName = in.next();
            Path oldFolder = workingDirPath.resolve(oldFolderName);
            if (cleaningDirectory(in, oldFolder)){
                return true;
            }
            System.out.println("Enter new folder name:");
            String newFolderName = in.next();
            Path newFolder = Paths.get(newFolderName);
            Files.move(oldFolder, oldFolder.resolveSibling(newFolder));
        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    private static boolean createNewDirectory(Scanner in, Path workingDirPath) {
        try {
            System.out.println("Enter folder name or path:");
            String folderName = in.next();
            if (Files.exists(workingDirPath.resolve(folderName))) {
                System.out.println("Folder with such name have been already exist." +
                        " Do you want to rewrite?(y/n)");
                String confirmReplacement = in.next();
                if (!confirmReplacement.equalsIgnoreCase("y")) {
                    return true;
                }
            }
            Files.createDirectories(workingDirPath.resolve(folderName));
        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    private static boolean cleaningDirectory(Scanner in, Path checkingFolder) throws IOException {
        if(Files.isDirectory(checkingFolder) && Files.list(checkingFolder).findFirst().isPresent()) {
            System.out.println("Directory isn't empty\n" +
                    "Do you want to clean it and continue?(y/n)");
            String confirmDeleting = in.next();
            if (confirmDeleting.equalsIgnoreCase("y")) {
                Files.walk(checkingFolder)
                        .filter(Files::isRegularFile)
                        .map(Path::toFile)
                        .forEach(File::delete);
            } else {
                return true;
            }
        }
        return false;
    }
}
