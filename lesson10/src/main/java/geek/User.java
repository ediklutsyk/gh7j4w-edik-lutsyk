package geek;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = -2017;

    private int id;
    private String name;
    private int accessLevel;
    private String favoriteEncoding;

    public User(int id, String name, int accessLevel, String favoriteEncoding) {
        this.id = id;
        this.name = name;
        this.accessLevel = accessLevel;
        this.favoriteEncoding = favoriteEncoding;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public String getFavoriteEncoding() {
        return favoriteEncoding;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", favoriteEncoding='" + favoriteEncoding + '\'' +
                ", accessLevel=" + accessLevel + '}';
    }
}
