package com.geekhub.lessons.task2;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Task {
    private String category;
    private String description;
    private LocalDateTime date;

    public Task(String category, String description, LocalDateTime date) {
        this.category = category;
        this.description = description;
        this.date = date;
    }

    public LocalDateTime getFullDate() {
        return date;
    }

    public LocalDate getDate() {
        LocalDate output = date.toLocalDate();
        return output;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Task{" +
                "category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
