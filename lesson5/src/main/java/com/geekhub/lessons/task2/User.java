package com.geekhub.lessons.task2;

import java.time.*;
import java.util.*;

public class User implements Organiser {
    private Map<LocalDateTime, Task> tasksMap = new HashMap<>();
    private Set<String> categoriesSet = new HashSet<>();

    @Override
    public void addTask(LocalDateTime date, Task task) {
        tasksMap.put(date, task);
    }

    @Override
    public void removeTask(LocalDateTime date) {
        tasksMap.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        tasksMap.values().forEach(item -> {
            categoriesSet.add(item.getCategory());
        });
        return categoriesSet;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> taskByCategories = new HashMap<>();
        categoriesSet.forEach(category -> {
            List<Task> tasksList = getTasksByCategory(category);
            taskByCategories.put(category, tasksList);
        });
        return taskByCategories;

    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasksList = new ArrayList<>();
        tasksMap.values().forEach(item -> {
            if (item.getCategory().equals(category)) {
                tasksList.add(item);
            }
        });
        tasksList.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        return tasksList;
    }

    @Override
    public List<Task> getTaskForToday() {
        LocalDate today = LocalDate.now();
        List<Task> tasksList = new ArrayList<>();
        tasksMap.values().forEach(item -> {
            if (item.getDate().equals(today)) {
                tasksList.add(item);
            }
        });
        tasksList.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        return tasksList;
    }
}
