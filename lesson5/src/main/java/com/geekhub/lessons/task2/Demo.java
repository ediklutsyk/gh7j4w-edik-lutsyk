package com.geekhub.lessons.task2;

import java.time.LocalDateTime;
import java.time.Month;

public class Demo {
    public static void main(String[] args) {
        User user = new User();
        Task task1 = new Task("Homework", "Math",
                LocalDateTime.of(2017, Month.NOVEMBER, 6, 15, 00, 00));
        Task task2 = new Task("Homework", "Informatics",
                LocalDateTime.of(2017, Month.NOVEMBER, 6, 17, 00, 00));
        Task task3 = new Task("Homework", "English",
                LocalDateTime.of(2017, Month.NOVEMBER, 5, 17, 00, 00));
        Task task4 = new Task("Household duties", "It would be nice to do",
                LocalDateTime.of(2017, Month.NOVEMBER, 6, 20, 00, 00));

        user.addTask(task1.getFullDate(), task1);
        user.addTask(task2.getFullDate(), task2);
        user.addTask(task3.getFullDate(), task3);
        user.addTask(task4.getFullDate(), task4);

        user.removeTask(LocalDateTime.of(2017, Month.NOVEMBER, 6, 17, 00, 00));

        System.out.println("Print all categories");
        System.out.println(user.getCategories());
        System.out.println("\nPrint all tasks for category \"Homework\"");
        System.out.println(user.getTasksByCategory("Homework"));
        System.out.println("\nPrint all task for every category");
        System.out.println(user.getTasksByCategories());
        System.out.println("\nPrint all tasks for today");
        System.out.println(user.getTaskForToday());
    }
}
