package com.geekhub.lessons.task2;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface Organiser {

    void addTask(LocalDateTime date, Task task);

    void removeTask(LocalDateTime date);

    Collection<String> getCategories();

    Map<String, List<Task>> getTasksByCategories();

    List<Task> getTasksByCategory(String category);

    List<Task> getTaskForToday();


}
