package com.geekhub.lessons.task1;

import java.util.*;

public class Demo {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Grey", "Mercedes", 2965));
        cars.add(new Car("Blue", "Mercedes", 2665));
        cars.add(new Car("White", "Mercedes", 2865));
        cars.add(new Car("Red", "Reno", 2690));
        cars.add(new Car("Blue", "Reno", 2540));
        cars.add(new Car("Green", "Reno", 2685));
        cars.add(new Car("Blue", "Honda", 2550));
        cars.add(new Car("Blue", "Honda", 2650));
        cars.add(new Car("White", "Honda", 2550));

        Comparator<Car> carComparator = (o1, o2) -> {
            int resultOfCompare;
            resultOfCompare = o1.getColor().compareTo(o2.getColor());
            if (resultOfCompare != 0) {
                return resultOfCompare;
            }
            resultOfCompare = o1.getBrand().compareTo(o2.getBrand());
            if (resultOfCompare != 0) {
                return resultOfCompare;
            }
            return o2.getBaseLength().compareTo(o1.getBaseLength());
        };

        cars.sort(carComparator);
        cars.forEach(System.out::println);

    }
}
