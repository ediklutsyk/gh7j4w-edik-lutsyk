package com.geekhub.lessons.task1;

public class Car {
    private String color;
    private String brand;
    private Integer baseLength;

    public Car(String color, String brand, Integer baseLength) {
        this.color = color;
        this.brand = brand;
        this.baseLength = baseLength;
    }

    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }

    public Integer getBaseLength() {
        return baseLength;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                ", baseLength=" + baseLength +
                '}';
    }

}
