package Task4;

import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class TaskFour {

    public static boolean isPrime(int n) {
        if (n < 2){
            return false;
        }
        for (int i = 2; i <= sqrt(n); i++){
            if (n % i == 0){
                return false;}
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        do {
            System.out.print("Enter your number:");
            n=in.nextInt();
        }
        while (!isPrime(n));
        System.out.print("Congrat");
    }
}
