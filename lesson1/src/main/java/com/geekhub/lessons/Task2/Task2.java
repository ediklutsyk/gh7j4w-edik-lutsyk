package Task2;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Task2 {
    public static int factorial(int n) {
        if (n > 1) {
            return n * factorial(n - 1);
        } else {
            return 1;
        }

    }

    public static void main(String[] args) {
        int n;
        boolean confirmBool=true;
        String confirm = "y";
        System.out.println("Enten N: ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        if (n < 0) {
            System.out.println("Error");
        } else if (n > 10) {
            System.out.println("Warning! The input number is too big, so this operation could take some time. ");
            System.out.println("If you want to continue input 'y' belong:");
            confirmBool =in.next().equalsIgnoreCase(confirm);
            if (!confirmBool) {
                System.out.println("Program work has been stopted");
            }
        }
        if(confirmBool) {
            System.out.println("Factorial for N=" + factorial(n));
        }
    }
}

/**
 * Attempt to get params as command line arguments during program start
 */
 /*
     public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String arg1 = args[0];
        int n = Integer.parseInt(arg1);
        String confirm = "y";
        System.out.println("Enten N: ");
        if (n < 0) {
            System.out.println("Error");
        } else if (n > 10) {
            System.out.println("Warning! The input number is too big, so this operation could take some time. ");
            System.out.println("If you want to continue input 'y' belong:");
            if (in.next().equalsIgnoreCase(confirm)) {
                System.out.println("Factorial for N=" + factorial(n));
            } else {
                System.out.println("Program work has been stopted");
            }
        } else {
            System.out.println("Factorial for N=" + factorial(n));
        }

    }
} */

