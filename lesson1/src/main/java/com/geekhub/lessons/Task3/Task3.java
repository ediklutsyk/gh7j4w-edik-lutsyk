package Task3;

public class Task3 {

    public static void main(String[] args) {
        int[] arr = new int[26];
        int n=65;
        char c;
        System.out.println("Uppercase:");
        for (int i = 0; i < arr.length; i++) {
            arr[i]=n;
            n++;
            c=(char)arr[i];
            System.out.print(c+" ");
        }
        System.out.println("\nLowercase:");
        for (int i = 0; i < arr.length; i++) {
            arr[i]+=32;
            c=(char)arr[i];
            System.out.print(c+" ");
        }
    }
}