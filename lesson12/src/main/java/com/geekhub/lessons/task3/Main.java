package com.geekhub.lessons.task3;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.lang.Math.pow;

public class Main {
    public static void main(String[] args) {
        ExecutorService service = null;
        List<String> urlList = InputUrl();
        try {
            service = Executors.newFixedThreadPool(20);
            Path tempDir = Files.createTempDirectory("tmp").normalize();
            Future s = null;
            for (String url : urlList) {
                s = service.submit(() -> {
                    try {
                        downloadLinks(url, String.valueOf(tempDir) + "\\" + urlList.indexOf(url) + ".html");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            while (!s.isDone()) {
            }
            System.out.println(countSizeAndDelete(tempDir));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (service != null) service.shutdown();
        }

    }

    public static Double countSizeAndDelete(Path tempDir) throws IOException {
        List<Long> sizesList = new ArrayList<>();
        Files.walk(tempDir, 1).forEach(file -> {
            try {
                sizesList.add(Files.size(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Double sizeInMegabytes = sizesList.stream().reduce((a, b) -> a + b).get() / pow(1024, 2);
        Files.walk(tempDir, 1).forEach(file -> {
            try {
                if (Files.isRegularFile(file)) {
                    Files.delete(file);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return sizeInMegabytes;
    }

    public static void downloadLinks(String url, String destination) throws IOException {
        URL source = new URL(url);
        try (InputStream in = source.openStream();
             OutputStream out = Files.newOutputStream(Paths.get(destination))) {
            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> InputUrl() {
        Path fileWithUrls = Paths.get("lesson12\\resources\\urlsForThirdTask.txt");
        List<String> urlsList = new ArrayList<>();
        try {
            urlsList = Files.readAllLines(fileWithUrls);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return urlsList;
    }
}
