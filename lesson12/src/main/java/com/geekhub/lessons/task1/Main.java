package com.geekhub.lessons.task1;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        List<String> urlList = InputUrl();
        ExecutorService service = null;
        URLChecker urlChecker = new URLChecker();
        try {
            service = Executors.newFixedThreadPool(urlList.size());
            for (String url : urlList) {
                service.submit(() -> {
                    try {
                        Map<Integer, Set<URL>> urls = urlChecker.check(url);
                        print(urls, url);
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                });
            }
        } finally {
            if (service != null) service.shutdown();
        }
    }

    private static List<String> InputUrl() {
        Path fileWithUrls = Paths.get("lesson12\\resources\\urlsForFirstTask.txt");
        List<String> urlsList = new ArrayList<>();
        try {
            urlsList = Files.readAllLines(fileWithUrls);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return urlsList;
    }

    private static void print(Map<Integer, Set<URL>> urls, String connectedLink) {
        System.out.println("\nAll links from: " + connectedLink);
        for (Map.Entry<Integer, Set<URL>> statusCode : urls.entrySet()) {
            for (URL url : statusCode.getValue()) {
                int code = statusCode.getKey();
                String link = url.toString();
                System.out.println("Status code " + code + "link " + link);
            }
        }
    }
}
