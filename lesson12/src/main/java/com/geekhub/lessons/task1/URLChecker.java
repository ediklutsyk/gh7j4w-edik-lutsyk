package com.geekhub.lessons.task1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class URLChecker {

    public Map<Integer, Set<URL>> check(String url) throws IOException {
        URL workUrl = new URL(url);
        HttpURLConnection httpURLConnection = openConnection(workUrl);
        Map<Integer, Set<URL>> urlList = getURLsFromContent(httpURLConnection);
        return urlList;
    }

    private Map<Integer, Set<URL>> getURLsFromContent(HttpURLConnection connection) throws IOException {
        System.out.println("Getting links from " + connection.getURL().toString());

        Map<Integer, Set<URL>> urls = new TreeMap<>();
        String url = connection.getURL().toString();

        Document document = Jsoup.connect(url).get();
        Elements links = document.select("a[href]");
        ExecutorService service = null;
        try {
            service = Executors.newCachedThreadPool();
            for (Element link : links) {
                URL linkUrl = new URL(link.attr("abs:href"));
                try{
                    int code = getStatusCode(linkUrl.toString());
                    putUrl(urls, code, linkUrl);
                }catch (IOException ex) {
                    putUrl(urls, 404, linkUrl);
                }

            }
        } finally {
            if (service != null) service.shutdown();
        }

        return urls;
    }

    private void putUrl(Map<Integer, Set<URL>> urls, Integer code, URL url) {
        Set<URL> urlList;
        if (null == urls.get(code)) {
            urlList = new HashSet<URL>(Collections.EMPTY_SET);
        } else {
            urlList = urls.get(code);
        }
        urlList.add(url);
        urls.put(code, urlList);
    }

    private int getStatusCode(String link) throws IOException {
        URL url = new URL(link);
        HttpURLConnection connection = openConnection(url);
        return connection.getResponseCode();
    }


    private HttpURLConnection openConnection(URL link) throws IOException {
        HttpURLConnection http = null;
        try {
            http = (HttpURLConnection) link.openConnection();
            int code = http.getResponseCode();
        } catch (Exception e) {
            throw new IOException("Bad url: " + e.getMessage());
        }
        return http;
    }
}
