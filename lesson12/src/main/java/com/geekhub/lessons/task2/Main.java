package com.geekhub.lessons.task2;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter directory to traverse: ");
        String workingDir = in.next();
        Path workingDirPath = Paths.get(workingDir);
        System.out.println("Enter pattern: ");
        String filePattern = in.next();
        String pattern = filePattern.substring(filePattern.indexOf('.'), filePattern.length());
        FindFiles find1 = new FindFiles(workingDirPath, pattern, 2);
        FindFiles find2 = new FindFiles(workingDirPath, pattern, 3);
        Thread tread1 = new Thread(find1);
        Thread tread2 = new Thread(find2);
        tread1.start();
        tread2.start();
        while (tread2.isAlive()) {
        }
        Set<String> resultSet = Stream.concat(find1.getFilePathsSet().stream(),
                find2.getFilePathsSet().stream()).collect(Collectors.toSet());
        resultSet.forEach(System.out::println);
    }

}
