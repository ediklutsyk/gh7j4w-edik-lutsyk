package com.geekhub.lessons.task2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class FindFiles implements Runnable {
    private Path workingDirPath;
    private String filePattern;
    private int maxDepth;
    private Set<String> filePathsSet = new HashSet<>();

    public FindFiles(Path workingDirPath, String filePattern, int maxDepth) {
        this.workingDirPath = workingDirPath;
        this.filePattern = filePattern;
        this.maxDepth = maxDepth;
    }

    public Set<String> getFilePathsSet() {
        return filePathsSet;
    }

    @Override
    public void run() {
        try (Stream<Path> stream = Files.find(workingDirPath, maxDepth,
                (path, attr) -> String.valueOf(path).endsWith(filePattern))) {
            stream.map(String::valueOf).forEach(path -> {
                filePathsSet.add(path);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
