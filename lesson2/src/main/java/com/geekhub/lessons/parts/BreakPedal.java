package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class BreakPedal implements StatusAware {
    protected String status;

    @Override
    public String getStatus() {
        return status;
    }

    public void slowDown(){
        this.status = "used";
        System.out.println("SLOWING DOWN");
    }
}
