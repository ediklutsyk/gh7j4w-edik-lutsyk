package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.Retarding;

public class RearWheel implements Retarding {
    @Override
    public void slowDown() {
        System.out.println("STOP");
    }
}
