package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class Engine implements StatusAware {
    private boolean status;
    private int power;
    private EngineType type;

    public Engine(int power, EngineType type) {
        this.power = power;
        this.type = type;
    }

    public void switchOff(){
        status = false;
    }

    public void switchOn(){
        status = true;

    }

    public int getPower() {
        return power;
    }

    public EngineType getType() {
        return type;
    }

    @Override
    public String getStatus(){
        return String.valueOf(status);
    }


}
