package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class Accelerator implements StatusAware {
    protected String status;

    @Override
    public String getStatus() {
        return status;
    }

    public void accelerate(){
        this.status = "used";
        System.out.println("SPEED UP!");
    }
}
