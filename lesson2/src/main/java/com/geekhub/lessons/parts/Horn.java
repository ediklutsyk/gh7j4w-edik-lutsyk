package com.geekhub.lessons.parts;

import com.geekhub.lessons.interfaces.StatusAware;

public class Horn implements StatusAware {
    private boolean status;

    public void switchHornOff(){
        status = false;
        System.out.println("Car isn't beeps");
    }

    public void switchHornOn(){
        status = true;
        System.out.println("Car is beeps");
    }
    @Override
    public String getStatus(){
        return String.valueOf(status);
    }


}
