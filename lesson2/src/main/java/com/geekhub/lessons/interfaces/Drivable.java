package com.geekhub.lessons.interfaces;

import com.geekhub.lessons.parts.*;

public interface Drivable {
    Accelerator getAccelerator();

    BreakPedal getBreakPedal();

    Engine getEngine();

    GasTank getGasTank();

    Horn getHorn();

    String getName();

    FrontWheel[] getFrontWheels();

    RearWheel[] getRearWheels();

}
