package com.geekhub.lessons.interfaces;

public interface StatusAware {
    String getStatus();
}
