package com.geekhub.lessons.interfaces;

public interface Steered {
    void turnLeft();
    void turnRight();
}