package com.geekhub.lessons.vehicls;

import com.geekhub.lessons.parts.*;

public class Hatchback3Doors extends Vehicle {

    public Hatchback3Doors(DriveType driveType, String name, Accelerator accelerator, BreakPedal breakPedal, Engine engine, GasTank gasTank, Horn horn) {
        super(3, driveType, name, accelerator, breakPedal, engine, gasTank, horn);
    }
}
