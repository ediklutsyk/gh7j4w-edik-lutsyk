package com.geekhub.lessons.vehicls;

import com.geekhub.lessons.parts.*;

public class Hatchback5Doors extends Vehicle {

    public Hatchback5Doors(DriveType driveType, String name, Accelerator accelerator, BreakPedal breakPedal, Engine engine, GasTank gasTank, Horn horn) {
        super(5, driveType, name, accelerator, breakPedal, engine, gasTank, horn);
    }
}
