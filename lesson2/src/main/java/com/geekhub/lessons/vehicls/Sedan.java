package com.geekhub.lessons.vehicls;

import com.geekhub.lessons.parts.*;

public class Sedan extends Vehicle {

    public Sedan( DriveType driveType, String name, Accelerator accelerator, BreakPedal breakPedal, Engine engine, GasTank gasTank, Horn horn) {
        super(4, driveType, name, accelerator, breakPedal, engine, gasTank, horn);
    }
}
