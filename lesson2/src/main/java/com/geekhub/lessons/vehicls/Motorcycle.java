package com.geekhub.lessons.vehicls;

import com.geekhub.lessons.interfaces.Drivable;
import com.geekhub.lessons.parts.*;

public class Motorcycle implements Drivable {
    private final String name;
    protected final Accelerator accelerator;
    private final BreakPedal breakPedal;
    protected final Engine engine;
    private final GasTank gasTank;
    protected final Horn horn;
    private final FrontWheel[] front = {new FrontWheel()};
    private final RearWheel[] rear = {new RearWheel()};

    public Motorcycle(String name, Accelerator accelerator,
                      BreakPedal breakPedal, Engine engine,
                      GasTank gasTank, Horn horn) {
        this.name = name;
        this.accelerator = accelerator;
        this.breakPedal = breakPedal;
        this.engine = engine;
        this.gasTank = gasTank;
        this.horn = horn;
    }

    @Override
    public Accelerator getAccelerator() {
        return accelerator;
    }

    @Override
    public BreakPedal getBreakPedal() {
        return breakPedal;
    }

    @Override
    public Engine getEngine() {
        return engine;
    }

    @Override
    public GasTank getGasTank() {
        return gasTank;
    }

    @Override
    public Horn getHorn() {
        return horn;
    }

    @Override
    public String getName() {
        return "Motorcycle";
    }

    @Override
    public FrontWheel[] getFrontWheels() {
        return new FrontWheel[0];
    }

    @Override
    public RearWheel[] getRearWheels() {
        return new RearWheel[0];
    }
}