package com.geekhub.lessons.vehicls;

import com.geekhub.lessons.parts.*;

public class Couple extends Vehicle {

    public Couple(DriveType driveType,
                  String name, Accelerator accelerator, BreakPedal breakPedal,
                  Engine engine, GasTank gasTank, Horn horn) {
        super(2, driveType, name, accelerator, breakPedal, engine, gasTank, horn);
    }
}
