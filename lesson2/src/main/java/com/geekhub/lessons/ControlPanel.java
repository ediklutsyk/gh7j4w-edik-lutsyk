package com.geekhub.lessons;

import com.geekhub.lessons.interfaces.Drivable;

public class ControlPanel {
    public void switchOn(Drivable drivable) {
        drivable.getEngine().switchOn();
    }

    public void switchOff(Drivable drivable) {
        drivable.getEngine().switchOff();
    }

    public void switchHornOn(Drivable drivable) {
        drivable.getHorn().switchHornOn();
    }

    public void switchHornOff(Drivable drivable) {
        drivable.getHorn().switchHornOff();
    }

    public void refuel(Drivable drivable) {
        drivable.getGasTank().setCurrentVolume(30);
    }

    public void accelerate(Drivable drivable) {
        drivable.getAccelerator().accelerate();
    }

    public void slowDown(Drivable drivable) {
        drivable.getBreakPedal().slowDown();
    }
}
