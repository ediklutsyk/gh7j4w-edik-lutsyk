package com.geekhub.lessons;

import com.geekhub.lessons.factories.*;
import com.geekhub.lessons.interfaces.Drivable;

public class Demo {
    public static void main(String[] args) {
        ControlPanel panel = new ControlPanel();
        for (DrivableFactory factory : factories()) {
            Drivable drivable = factory.create();
            System.out.println("Test Drive: " + drivable.getName());
            panel.refuel(drivable);
            System.out.println("Refuel "+drivable.getGasTank().toString());
            panel.switchOn(drivable);
            System.out.println("Engine work status:"+drivable.getEngine().getStatus());
            panel.accelerate(drivable);
            System.out.println("Accelerator "+drivable.getAccelerator().getStatus());
            panel.slowDown(drivable);
            System.out.println("Break pedal "+drivable.getBreakPedal().getStatus());
            panel.switchOff(drivable);
            System.out.println("Engine work status:"+drivable.getEngine().getStatus());
            panel.switchHornOn(drivable);
            System.out.println("Test Drive Successfully Finished.");
            System.out.println("-----------------------------------");
        }
    }

    private static DrivableFactory[] factories() {
        return new DrivableFactory[] {
                new SedanFactory(),
                new SuvFactory(),
                new CoupeFactory(),
                new Hatchback3Factory(),
                new Hatchback5Factory(),
                new MotorcycleFactory()
        };
    }
}