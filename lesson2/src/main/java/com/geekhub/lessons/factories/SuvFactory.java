package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.*;
import com.geekhub.lessons.vehicls.SUV;

public class SuvFactory implements DrivableFactory {
    @Override
    public SUV create() {
        return new SUV(DriveType.FULL_DRIVE,"SAW4"
                , new Accelerator(),new BreakPedal(),new Engine(160,EngineType.DIESEL),
                new GasTank(60),new Horn());
    }
}
