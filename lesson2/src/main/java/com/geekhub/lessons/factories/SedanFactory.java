package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.*;
import com.geekhub.lessons.vehicls.Sedan;

public class SedanFactory implements DrivableFactory {
    @Override
    public Sedan create() {
        return new Sedan(DriveType.FRONT_DIVE,"Lancer X"
                , new Accelerator(),new BreakPedal(),new Engine(150,EngineType.GAS),
                new GasTank(50),new Horn());
    }
}
