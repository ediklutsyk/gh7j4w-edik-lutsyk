package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.*;
import com.geekhub.lessons.vehicls.Hatchback3Doors;

public class Hatchback3Factory implements DrivableFactory {
    @Override
    public Hatchback3Doors create() {
        return new Hatchback3Doors(DriveType.FRONT_DIVE,"Daevo"
                , new Accelerator(),new BreakPedal(),new Engine(180,EngineType.GAS),
                new GasTank(40),new Horn());
    }
}
