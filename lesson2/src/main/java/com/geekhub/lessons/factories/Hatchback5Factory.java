package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.*;
import com.geekhub.lessons.vehicls.Hatchback5Doors;

public class Hatchback5Factory implements DrivableFactory {
    @Override
    public Hatchback5Doors create() {
        return new Hatchback5Doors(DriveType.FRONT_DIVE,"Daevo cielo"
                , new Accelerator(),new BreakPedal(),new Engine(180,EngineType.GAS),
                new GasTank(45),new Horn());
    }
}
