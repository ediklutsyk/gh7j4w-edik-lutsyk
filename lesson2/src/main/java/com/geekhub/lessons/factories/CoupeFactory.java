package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.*;
import com.geekhub.lessons.vehicls.Couple;

public class CoupeFactory implements DrivableFactory {
    @Override
    public Couple create() {
        return new Couple(DriveType.FRONT_DIVE,"Mercedes-Benz"
                , new Accelerator(),new BreakPedal(),new Engine(200,EngineType.GAS),
                new GasTank(50),new Horn());
    }
}
