package com.geekhub.lessons.factories;

import com.geekhub.lessons.parts.*;
import com.geekhub.lessons.vehicls.Motorcycle;

public class MotorcycleFactory implements DrivableFactory {
    @Override
    public Motorcycle create() {
        return new Motorcycle("Harley-Davidson"
                , new Accelerator(), new BreakPedal(), new Engine(200, EngineType.GAS),
                new GasTank(30), new Horn());
    }
}
