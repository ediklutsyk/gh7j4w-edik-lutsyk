package com.geekhub.lessons;

import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class MorseTranslator {

    public String translate(String userInput){
        if (userInput.isEmpty() || !userInput.matches(".*\\w.*")){
            throw new IllegalArgumentException("Input shouldn't be empty");
        } else {
            return userInput.toUpperCase().chars().mapToObj(c->charToMorse((char)c)).collect(Collectors.joining(" "));
        }
    }

    private String charToMorse(char ch){
        switch (ch){
            case ' ': return "/";
            case 'A': return ".-";
            case 'B': return "-...";
            case 'C': return "-.-.";
            case 'D': return "-..";
            case 'E': return ".";
            case 'F': return "..-.";
            case 'G': return "--.";
            case 'H': return "....";
            case 'I': return "..";
            case 'J': return ".---";
            case 'K': return "-.-";
            case 'L': return ".-..";
            case 'M': return "--";
            case 'N': return "-.";
            case 'O': return "---";
            case 'P': return ".--.";
            case 'Q': return "--.-";
            case 'R': return ".-.";
            case 'S': return "...";
            case 'T': return "-";
            case 'U': return "..-";
            case 'V': return "...-";
            case 'W': return ".--";
            case 'X': return "-..-";
            case 'Y': return "-.--";
            case 'Z': return "--..";
            case '1': return ".----";
            case '2': return "..---";
            case '3': return "...--";
            case '4': return "....-";
            case '5': return ".....";
            case '6': return "-....";
            case '7': return "--...";
            case '8': return "---..";
            case '9': return "----.";
            case '0': return "-----";
            default: throw new IllegalArgumentException("Entered char isn`t letter or number");
        }
    }
}
