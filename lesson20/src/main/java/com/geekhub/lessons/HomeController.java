package com.geekhub.lessons;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    private final MorseTranslator translator;

    public HomeController(MorseTranslator translator) {
        this.translator = translator;
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping(value = "session")
    public String toSession(@RequestParam("input") String input, Model model) {
        model.addAttribute("output",translator.translate(input));
        return "index";
    }
}
