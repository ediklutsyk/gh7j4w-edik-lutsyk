package com.geekhub.lessons;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MorseTranslatorTest {
    private MorseTranslator translator;

    @BeforeMethod
    public void setUp() {
        translator = new MorseTranslator();
    }

    @DataProvider(name = "correctStrings")
    public static Object[][] correctStrings() {
        return new Object[][]{
                {"SOS", "... --- ..."},
                {"S O S", "... / --- / ..."},
                {"GeekHub", "--. . . -.- .... ..- -..."},
                {"it is a good day to die", ".. - / .. ... / .- / --. --- --- -.. / -.. .- -.-- / - --- / -.. .. ."},
                {"0 1 2 3 4 5 6 7 8 9", "----- / .---- / ..--- / ...-- / ....- / ..... / -.... / --... / ---.. / ----."},
                {"A B C D E F G H I J K L M N O P Q R S T U V W X Y Z",
                                ".- / -... / -.-. / -.. / . / ..-. / --. / .... / .. / .--- /" +
                                " -.- / .-.. / -- / -. / --- / .--. / --.- / .-. / ... /" +
                                " - / ..- / ...- / .-- / -..- / -.-- / --.."}

        };
    }

    @Test(dataProvider = "correctStrings")
    public void testTranslateSuccess(String input, String expectedAnswer) {
        String answer = translator.translate(input);
        assertEquals(answer, expectedAnswer);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTranslateFail() {
        String answer = translator.translate("$0m*th!#g Br0k*#!");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTranslateEmpty() {
        String answer = translator.translate("");
    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTranslateOnlySpaces() {
        String answer = translator.translate("            ");
    }
}