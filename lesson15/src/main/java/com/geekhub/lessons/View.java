package com.geekhub.lessons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class View {
    public static void responseOutput(PrintWriter out, HttpServletRequest req) {
        Map<String, String> users;
        HttpSession session = req.getSession();
        if (session == null || session.getAttribute("map") == null) {
            users = new HashMap<>();
        } else {
            users = (Map<String, String>) session.getAttribute("map");
        }
        out.write("<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\">\n" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n" +
                "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css\" integrity=\"sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy\" crossorigin=\"anonymous\">\n" +
                "<link rel=\"stylesheet\" href=\"main.css\">\n" +
                "<title>Hello, world!</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"container\">\n" +
                "</br>" +
                "<form action=\"/session\" method=\"get\">\n" +
                "<div class=\"form-group row\">\n" +
                "<label class=\"col-1\" for=\"actionInput\">Action:</label>\n" +
                "<div class=\"col-4\">\n" +
                "<select multiple class=\"form-control\" name=\"action\" >\n" +
                "<option value=\"\"></option>\n" +
                "<option value=\"add\">Add</option>\n" +
                "<option value=\"update\">Update</option>\n" +
                "<option value=\"invalidate\">Invalidate</option>\n" +
                "<option value=\"delete\">Remove</option>\n" +
                "</select>\n" +
                "</div>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"form-group row\">\n" +
                "<label class=\"col-1\" for=\"nameInput\">Name:</label>\n" +
                "<div class=\"col-8\">\n" +
                "<input class=\"form-control\" type=\"text\" name=\"name\" placeholder=\"Enter name\" required>\n" +
                "</div>\n" +
                "\n" +
                "</div>\n" +
                "<div class=\"form-group row\">\n" +
                "<label class=\"col-1\" for=\"valueInput\">Value:</label>\n" +
                "<div class=\"col-8\">\n" +
                "<input class=\"form-control \" type=\"text\" name=\"value\" placeholder=\"Enter value\" required>\n" +
                "</div>\n" +
                "\n" +
                "</div>\n" +
                "<input class=\"btn btn-outline-primary\" type=\"submit\" value=\"Submit\">\n" +
                "</form>\n" +
                "<hr>\n" +
                "<table class=\"table\">\n" +
                "<thead class=\"thead-dark\">\n" +
                "<tr>\n" +
                "<th scope=\"col\">#</th>\n" +
                "<th scope=\"col\">Name</th>\n" +
                "<th scope=\"col\">Value</th>\n" +
                "</tr>\n" +
                "</thead>\n" +
                "<tbody>\n");
        final AtomicInteger i = new AtomicInteger(1);
        users.forEach((k, v) -> {
            out.write("<tr>\n" +
                    "<th scope=\"row\">" + i.getAndAdd(1) + "</th>\n" +
                    "<td>" + k + "</td>\n" +
                    "<td>" + v + "</td>\n" +
                    "</tr>\n");
        });
        out.write("" +
                "</tbody>\n" +
                " </table>\n" +
                "</div>" +
                "<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\n" +
                "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>\n" +
                "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js\" integrity=\"sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4\" crossorigin=\"anonymous\"></script>\n" +
                "</body>\n" +
                "</html>");
    }
}
