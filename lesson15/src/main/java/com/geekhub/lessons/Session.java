package com.geekhub.lessons;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/session", name = "session")
public class Session extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, String> users;
        HttpSession session = req.getSession();
        if (session == null || session.getAttribute("map") == null) {
            users = new HashMap<>();
        } else {
            users = (Map<String, String>) session.getAttribute("map");
        }
        PrintWriter out = resp.getWriter();
        switch (req.getParameter("action")) {
            case "add":
                users.put(req.getParameter("name"), req.getParameter("value"));
                break;
            case "update":
                users.replace(req.getParameter("name"), req.getParameter("value"));
                break;
            case "delete":
                users.remove(req.getParameter("name"));
                break;
            case "invalidate":
                users.clear();
                break;
        }
        session.setAttribute("map", users);
        View.responseOutput(out, req);
    }
}
