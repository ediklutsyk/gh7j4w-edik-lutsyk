package com.geekhub.lessons.task1;

import com.geekhub.lessons.task1.exceptions.NumberFormatException;

public class Demo {
    public static void main(String[] args) {
        parseIntMethod("1a45", 1);
        parseIntMethod("-0145", 2);
        parseIntMethod("0145", 3);
        parseIntMethod("145", 4);
        parseIntMethod("-145", 5);
    }

    private static void parseIntMethod(String stringToParse, int numberOfExample) {
        try {
            System.out.println("Example "+numberOfExample + ": " + Integer.parseInt(stringToParse));
        } catch (NumberFormatException e) {
            System.out.println("Example "+numberOfExample + ": " + e.getMessage());
        }
    }
}
