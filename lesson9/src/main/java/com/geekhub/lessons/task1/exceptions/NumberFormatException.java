package com.geekhub.lessons.task1.exceptions;

public class NumberFormatException extends ParseException {
    public NumberFormatException() {
    }

    public NumberFormatException(String message) {
        super(message);
    }

    public NumberFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
