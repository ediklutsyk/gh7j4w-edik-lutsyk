package com.geekhub.lessons.task1;

import com.geekhub.lessons.task1.exceptions.NumberFormatException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.pow;

public class Integer {

    public static int parseInt(String str) throws NumberFormatException {
        if (str.isEmpty()){
            throw new NumberFormatException("String is empty");
        }
        char[] charArray = str.toCharArray();
        List<Character> charList = new ArrayList<>();
        for (char element : charArray) {
            charList.add(element);
        }
        if (charList.get(0).equals('-')) {
            charList.remove(0);
            return -(charListToInt(charList));
        } else {
            return charListToInt(charList);
        }
    }

    private static int charListToInt(List<Character> charList) throws NumberFormatException {
        int result = 0;
        if (charList.get(0).equals('0')) {
            throw new NumberFormatException("Number can't start with zero");
        }
        Collections.reverse(charList);
        for (int i = 0; i < charList.size(); i++) {
            if (Character.isDigit(charList.get(i))) {
                result += (charList.get(i) - '0') * pow(10, i);
            } else {
                throw new NumberFormatException("String contains not number characters");
            }
        }
        return result;
    }
}
